---
title: "tale"
subtitle: "Requirements Specification"
author:
    - Marius Schär
    - Severin Kaderli
extra-info: true
institute: "Bern University of Applied Sciences"
department: "Engineering and Information Technology"
lecturer: "Joachim Wolfgang Kaltz"
lang: "en-UK"
toc: true
lot: true
rule-color: CDDC39
link-color: 448AFF
glossary: true
glossary-file: glossary.tex
...

# Purpose of this Document
This document describes the requirements for the "tale" project.
It is the output of the BTI7082 - Project Management module,
and thus follows the constraints set by it.

This document assumes that the reader has an understanding of how VCS
are used and the terminology surrounding them.
We will specifically re-use a lot of terminology from Git,
as it sees widespread use and the developers of "tale" are familiar with it.

More information on Git can be found

- on the [Git website](https://git-scm.com) or
- on ["Think like a Git"](https://think-like-a-git.net), a tutorial on Git.

# Vision
\begin{center}
\begin{Huge}
tale
\end{Huge}
\end{center}
is the software we'd like to build in this project. It's a VCS, which is
inspired by "Git". We will attempt to imitate the functionality of Git in order
to learn which problems and complexity must be overcome in the various
components of a VCS.

Our main focus will be the versioning of text content. We will look to Git for
its elegant graph model and CLI but will not look at any of the code,
instead trying to develop it ourselves.

The name "tale" stems from the purpose of a VCS: to tell the tale of versions.

The goal of the project is explicitly *not* to

- build a drop-in replacement for Git
- rebuild Git as closely as possible
- build "tale" to work together with Git

# Project Goals
The main goal of this project is to create a self hosting VCS with some of the
basic features of Git.

## Stakeholder
The stakeholders for the project are listed in the following table:

\begin{tab}{3}{Stakeholder}{Role & Shorthand & People}
    Bern University of Applied Science & BFH & G. Schwab \& W. J. Kaltz \\ \hline
    Developer & DEV & S. Kaderli \& M. Schär \\ \hline
    tale User & USR & S. Kaderli \& M. Schär
\end{tab}

## Project Goals
### Developer
The developers would like tale to be a self-hosting VCS. In the process of
developing tale, the developers want to learn about VCS internals and improve
their software development skills.

### `tale` User
The users of tale want to be able to use the application without problems and be
able to version text content.

### Bern University of Applied Sciences
The BFH wants the students taking the BTI7082 and BTI7301 modules to gather
practical experience in leading and executing a project successfully.


# System Scope
## Process Environment
The following table lists all the relevant core-processes for the application.
The processes are listed together with the corresponding command for the
functionality.

\begin{tab}{3}{Core Processes}{ID & Command & Description}
 P.1   & `init`                        & Create an empty repository.                     \\ \hline
 P.2   &                               & Record changes in the repository.                   \\ \hline
 P.2.1 & `add <file>`                  & Add file contents to the index.                     \\ \hline
 P.2.2 & `commit`                      & Record changes to the repository with a message.    \\ \hline
 P.3   & `log`                         & Show past commits with different levels of detail.  \\ \hline
 P.4   & `diff --no-index <a> <b>`     & Show difference between any two files.              \\ \hline
 P.5   & `diff`                        & Show difference between current changes and commit. \\ \hline
 P.6   & `revert <commit>`             & Undo a given commit.                                \\ \hline
 P.7   & `apply <diff>`                & Applies a given diff to the working tree.           \\ \hline
 P.8   & `branch <name>`               & Creates a new branch with the given name.           \\ \hline
 P.9   & `checkout <branch or commit>` & Switches to a given branch or commit.               \\ \hline
 P.10  & `merge <branch>`              & Merges the given branch into the current one.       
\end{tab}

\newpage
## System Environment
As tale is an application for local use there are not many systems that interact
with it. The main system interactions are with the filesystem, on which tale operates,
as well as the terminal emulator (or CLI) with which tale interacts with the user.

![System Context](./assets/system-context-diagram.png)

It is planned to make tale compatible with the most common operating systems:
Linux, Windows and MacOS.
As tale is written in Java and only features a CLI,
this compatibilty should be fairly easy to achieve.

Because the developers both work on Linux systems,
only that will be offically supported.

## Unsupported Project Goals
The main goal of the project is to create a functional application with just the
main features of a VCS. Currently there are no plans to introduce networking
features like pushing / pulling code to / from a remote repository. This can be
realized in future projects after the main functionality has been realized.

As stated above, tale will not be a replacement for Git or
be compatible with Git.

# Requirements
## Information Sources and Techniques
Our functional requirements were inspired by a basic Git workflow.
These come from our own experience as software developers who use Git daily.

\newpage 

## Functional Requirements

### Use Cases
The use cases for tale are shown in the following diagrams.
The detailed user stories are found in the following chapter.

![Use Cases - Displaying](./assets/tale_use_cases_display.png)

![Use Cases - Modifying](./assets/tale_use_cases_modifying.png)

![Use Cases - Branches](./assets/tale_use_cases_branches.png)

![Use Cases - miscellaneous](./assets/tale_use_cases_misc.png)

\newpage

### Detailed Requirements
#### U1 - Create a repository
As a tale user, I want to initialize a new repository in a directory.

**AC:**

- the command `tale init` initializes an empty repository
- a repository can be initialized in any directory that doesn't
already contain a repository
- all the data for the repository is stored in a .tale sub-directory
- the .tale directory should be self-contained

#### U2 - Add contents
**U2.1:** As a tale user, I want to add newly created files to the index.  
**U2.2:** As a tale user, I want to add changes in an existing file to the index.

**AC:**

- the command `tale add <file>` adds the new contents of `<file>` to the index.

#### U3 - Show differences
**U3.1:** As a tale user, I want to see the difference between any two files.  
**U3.2:** As a tale user, I want to see the difference between my working tree
and the last version in the index.

**AC:**

  - the command `tale diff` displays the difference between the working tree
and the last version in the index.
  - the command `tale diff --cached` displays the difference between the index,
and the HEAD
  - the command `tale diff --no-index <file_a> <file_b>` displays the differene between
the two given files, regardless of index status.
  - the differences should be displayed in [Unified Format](https://en.wikipedia.org/wiki/Diff#Unified_format)

\newpage

#### U4 - Create new branches
As a tale user, I want to create new branches.

**AC:**

- the command `tale branch <name>` creates a new branch named `<name>`

#### U5 - Record changes
As a tale user, I want to record changes
which have previously been added to the index.

**AC:**

- the command `tale commit -m "<message>"` creates a new commit with
the specified message
- the created commit has an ID which is a hash generated from:
  * the previous commit ID
  * the date and time
  * the author
  * the contents in this commit
  * the given `<message>`
- if no message is given, an error is displayed
- if no author is set, the system username is used
- if there is no previous commit ID, `0` is used

#### U6 - Switch to specific version or branch
**U6.1:** As a tale user, I want to switch to a different branch.  
**U6.2:** As a tale user, I want to switch to a specific version.

**AC:**

- the command `tale checkout <branch>` switches to the branch named `<branch>`
- the command `tale checkout <commit>` switches to the commit with ID `<commit>`
- if the specified destination is already checked out, a notice is displayed
- if a conflict arises because of a switch, the switch is not performed
and an error is displayed

#### U7 - Apply diffs
As a tale user, I want to apply patches in the
[Unified Format](https://en.wikipedia.org/wiki/Diff#Unified_format)
from a given file.

**AC:**

- the command `tale apply <patch>` applies the changes in the given patch file
- if the changes are not applicable, an error is displayed

#### U8 - Merge a branch into the current one
As a tale user, I want to merge another branch into the current one.

**AC:**

- the command `tale merge <branch>` adds the commits from the given `<branch>`
to the current one.
- if a conclict arises during application of one of the commits,
the merge is halted, and an error message is displayed
- to resolve a conflict, the offending files must be edited
and the new state commited
- the command `tale merge --continue` resumes merging after the conflict is resolved

#### U9 - Undo commits
As a tale user, I want to create a revert commit,
which undoes the changes in a previous commit.  

**AC:**

- the command `tale revert <commit>` creates a new commit
which reverses the changes in the given `<commit>`

\newpage

#### U10 - Show history
As a tale user, I want to see the current history.

**AC:**

- the command `tale history` displays the last commits in chronological order
- for each commit the following information is shown:
  * ID
  * message
  * date
  * author
- if the tip of a branch is at a commit, the branch name is shown
- the HEAD commit is specially marked

#### U11 - Show status of repository
As a tale user, I want to see the current status of the repository

**AC:**

- the command `tale status` displays the current status of the repository
- the status consists of the following information:
  * current branch
  * staged changes
  * changed files
  * new files
- the status also contains some help on how to proceed to commit the changes

\newpage

#### U12 - Reset to specific commit
As a tale user, I want to change the HEAD to a specific commit

**AC:**

- the command `tale reset <commit>` resets the HEAD to the specified `<commit>`
- instead of a commit-ID it's also possible to specify a commit relative to HEAD
  * this uses the syntax `HEAD~x`, where x is the commit x before HEAD
  * for example `HEAD~1` refers to the commit immediately preceding HEAD
- `tale reset` supports both `--soft` and `--hard`
  * `--soft` moves the HEAD and adds the changes between the HEAD positions to the staging area
  * `--hard` just moves the HEAD

\newpage

## Quality Requirements
\begin{tab}{4}{Quality Requirements}{ID & Status & Title & Description}
    Q1.1 & Draft & Responsiveness & tale should carry out all operations in under 5 seconds \\ \hline
    Q1.2 & Draft & Responsiveness & tale should show differences in under 1 second \\ \hline
    Q1.3 & Draft & Responsiveness & tale should apply patches in under 1 second \\ \hline
    Q1.4 & Draft & Responsiveness & tale should switch branch in under 1 second \\ \hline
    Q2.1 & Draft & Correctness & tale should reproduce history exactly \\ \hline
    Q2.2 & Draft & Correctness & tale should produce diffs correctly \\ \hline
    Q3.1 & Draft & Efficiency & tale should produce space-efficient diffs
    
\end{tab}

## Constraints
\begin{tab}{4}{Constraints}{ID & Status & Title & Description}
    C1.1 & Draft & User Interface & tale uses a command line interface \\ \hline
    C1.2 & Draft & User Interface & tale is documented in English \\ \hline
    C2.1 & Draft & Implementation & tale is implemented in Java \\ \hline
    C3.1 & Draft & Legal & tale is open source under the MIT license
\end{tab}

\newpage

## Business Object Model
Below we describe the model for tale.
On the left, the file system view of the `.tale` directory is shown,
on the right, it's explained further.

\colbegin[2]

### On the filesystem
\begin{forest}
  for tree={
    font=\ttfamily,
    grow'=0,
    child anchor=west,
    parent anchor=south,
    anchor=west,
    calign=first,
    edge path={
      \noexpand\path [draw, \forestoption{edge}]
      (!u.south west) +(7.5pt,0) |- node[fill,inner sep=1.25pt] {} (.child anchor)\forestoption{edge label};
    },
    before typesetting nodes={
      if n=1
        {insert before={[,phantom]}}
        {}
    },
    fit=band,
    before computing xy={l=15pt},
  }
[.tale
  [objects
    [staged]
    [78
      [78af39]
      [78872c]
    ]
    [d3
      [d3245f]
    ]
    [...]
  ]
  [HEAD]
  [branches
    [feature
      [diffing]
      [cli]
    ]
    [bugfix
      [issue-3]
    ]
  ]
  [index]
  [config.toml]
]
\end{forest}

\columnbreak

### object
An object is a file with the contents as described in the graphic below.
The first line contains the parent commit ID,
with an optional comma-separated ID of the second parent
(a merge commit has two parents).  
The second line contains the unix timestamp of the creation of this object.  
The rest of the file contains the author, commit message,
and the list of diffs associated with this commit
in a base64 encoding.

The name of the file is the SHA-256 digest of the bytes contained in the file.

\centering
![Object Model](./assets/object_model.png){width=3cm}

The objects are contained in a folder structure,
which makes for a rudimentary hashtable for faster search.

\colend

\newpage

### staged
`staged` is a special case of an object.
It contains the files which where added with `tale add`.
It has the HEAD as the parent-ID.
The timestamp is whenever it was last changed.
It contains an author, but no commit message.  
When `tale commit` is executed,
the staged object is converted into a real commit object by adding a message,
updating the timestamp,
and moving it to correct location.  
`staged` then gets re-created in its initial state.

### HEAD
`HEAD` contains only a branch name of that branch.
If we're in a detached-head state
(the current commit is not at the tip of any branch),
the HEAD contains that commit-ID.

### branches
The leaves of the `branches` sub-structure contain the commit-ID 
at which the current branch sits
(the commit at the tip of the respective branch).

### index
The `index` file contains a list of all the files known to tale,
as well as the commit-ID with which they were last changed.

### config.toml
This file contains the configuration for this tale repository.
It stores:

- author name (by default the username@hostname of the system)

The config is intended to be expanded in the future if needed.

\newpage

### Picture
The following picture shows the whiteboard used to plan out the business object
model:

\centering
![Whiteboard](./assets/whiteboard_full.jpg){height=18.5cm}

# Bibliography

# Attachments

## Schedule
We will implement tale in 5 sprints, which are listed below.
The 5th and last sprint is purposely left blank as reserve time
and to do some cleanup tasks.

\begin{tab}{5}{Sprint contents}{Sprint & Start Date & End Date & ID & Description}
  \textbf{Sprint 1} & 2019-10-21 & 2019-11-03 & U1 & Create a repository \\ \hline
  \multicolumn{3}{|c|}{} & U2 & Add contents \\ \hline
  \multicolumn{3}{|c|}{} & U5 & Record changes \\ \hline
  \multicolumn{3}{|c|}{} & U11 & Show status of repository \\ \hline
  \textbf{Sprint 2} & 2019-11-04 & 2019-11-17 & U3 & Show differences \\ \hline
  \multicolumn{3}{|c|}{} & U7 & Apply differences \\ \hline
  \multicolumn{3}{|c|}{} & U10 & Show history \\ \hline
  \multicolumn{3}{|c|}{} & U12 & Reset to specific commit \\ \hline
  \textbf{Sprint 3} & 2019-11-18 & 2019-11-01 & U6 & Switch to a specific version/branch \\ \hline
  \multicolumn{3}{|c|}{} &  U9 & Undo commits \\ \hline
  \textbf{Sprint 4} & 2019-12-02 & 2019-12-15 & U4 & Create new branches \\ \hline
  \multicolumn{3}{|c|}{} &  U8 & Merge a branch into the current one \\ \hline
  \textbf{Sprint 5} & 2019-12-02 & 2019-12-15 & & Cleanup and time reserves
\end{tab}

\newpage

## Definition of Ready
- [ ] All developers have understood the requirements and acceptance criteria
- [ ] All dependencies are resolved

## Definition of Done
- [ ] The acceptance criteria are met
- [ ] The code is tested automatically ($\geq$ 75% Test Coverage)
- [ ] The documentation is up to date
- [ ] The code is reviewed by another developer
- [ ] The code is merged into the master branch

# Version Control
The version control is located in the
[tale-doc Repository](https://gitlab.com/martyschaer/tale-doc) on GitLab.

The version history of this document can be read with the following command:

`git log -- documents/requirements-specification.md`
