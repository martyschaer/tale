<?php
declare(strict_types=1);

use App\Application\Controller\CategoryController;
use App\Application\Controller\ProductController;
use App\Application\Controller\UserController;
use Firebase\JWT\JWT;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {
  $container = $app->getContainer();
  $jwt_secret = $container->get('settings')['jwt']['secret'];

  $app->get('/', function (Request $request, Response $response) {
    $response->getBody()->write('Hello world!');
    return $response;
  });

  // Products
  $app->group('/products', function (RouteCollectorProxy $group) {
    $group->get('/', ProductController::class . ':index');
    $group->get('/{id}', ProductController::class . ':get');
  });

  // Categories
  $app->group('/categories', function (RouteCollectorProxy $group) {
    $group->get('/', CategoryController::class . ':index');
    $group->get('/{id}', CategoryController::class . ':get');
  });

  // Categories
  $app->group('/users', function (RouteCollectorProxy $group) {
    $group->get('/', UserController::class . ':index');
    $group->get('/{id}', UserController::class . ':get');
  });


  $app->post('/login', function (Request $request, Response $response) use ($jwt_secret) {
    $token = JWT::encode(['data' => 1], $jwt_secret, "HS256");
    return jsonResponse($response, ['token' => $token]);
  });
};

