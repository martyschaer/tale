package dev.tale.cli;

import dev.tale.cli.patch.Differ;
import dev.tale.cli.util.FileUtil;

import java.nio.file.Path;
import java.nio.file.Paths;

public class Application {
    public static void main(String[] args) throws Exception {
        Path aPath = Paths.get(args[0]).toAbsolutePath();
        Path bPath = Paths.get(args[1]).toAbsolutePath();

        Differ d = new Differ(FileUtil.loadFileFromPath(aPath), FileUtil.loadFileFromPath(bPath));
        System.out.println(d.generate());

        if(args.length == 3 && args[2].equals("-V")) {
            System.out.println("Debug output:");
            System.out.println(d.toString());
        }

    }
}

