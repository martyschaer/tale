package dev.tale.cli.function;


import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ZipServiceTest {

    @Test
    public void testDeflateInflate() {
        ZipService service = new ZipService();
        service.init();

        String expected = "Hello, Wörld, This is a string.\nAnother\0line ☕\n";
        byte[] input = expected.getBytes();

        byte[] deflated = service.deflate(input);
        byte[] inflated = service.inflate(deflated);

        assertThat(new String(inflated), is(equalTo(expected)));
    }
}
