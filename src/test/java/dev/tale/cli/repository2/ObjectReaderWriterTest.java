package dev.tale.cli.repository2;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.function.FileService;
import dev.tale.cli.repository2.object.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ObjectReaderWriterTest {

    private ObjectReader reader;

    private ObjectWriter writer;

    private static FileService fileService;

    @BeforeAll
    public static void setupMock() {
        fileService = new TestFileService();
        ServiceFactory.putMock(FileService.class, fileService);
    }

    @BeforeEach
    public void setup() {
        reader = ServiceFactory.get(ObjectReader.class);
        writer = ServiceFactory.get(ObjectWriter.class);
    }

    @Test
    public void testWriteReadObject() {
        Blob blob = new Blob("\t\0\fFirst line\nSecond line\n".getBytes());
        String object = writer.writeObject(blob);

        TaleObject actual = reader.readObject(ObjectType.BLOB, object);

        assertThat(actual, equalTo(blob));
    }

    @Test
    void testDetermineTypeBlob() {
        Blob blob = new Blob("\t\0\fFirst line\nSecond line\n".getBytes());
        String object = writer.writeObject(blob);

        ObjectType type = reader.determineType(object);
        assertThat(type, equalTo(ObjectType.BLOB));
    }

    @Test
    void testDetermineTypeTree() {
        Tree tree = new Tree();
        String object = writer.writeObject(tree);

        ObjectType type = reader.determineType(object);
        assertThat(type, equalTo(ObjectType.TREE));
    }

    @Test
    void testDetermineTypeCommit() {
        Commit commit = new Commit();
        commit.setTree("fff");
        commit.setTimestamp(1L);
        commit.setMessage("Initial commit");
        commit.setAuthor("author");
        String object = writer.writeObject(commit);

        ObjectType type = reader.determineType(object);
        assertThat(type, equalTo(ObjectType.COMMIT));
    }
}