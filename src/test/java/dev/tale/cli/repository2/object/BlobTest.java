package dev.tale.cli.repository2.object;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class BlobTest {
    @Test
    public void serializeBlob() {
        Blob expected = new Blob();
        expected.setContent("First Line\nSecond Line\r\nThird Line\n".getBytes());

        byte[] serialized = expected.serialize();

        Blob actual = new Blob().deserialize(serialized);

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void serializeEmptyBlob() {
        Blob expected = new Blob();
        expected.setContent(new byte[0]);

        byte[] serialized = expected.serialize();

        Blob actual = new Blob().deserialize(serialized);

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void serializeNullBlob() {
        Blob expected = new Blob();

        byte[] serialized = expected.serialize();

        Blob actual = new Blob().deserialize(serialized);

        assertThat(actual, equalTo(expected));
    }
}
