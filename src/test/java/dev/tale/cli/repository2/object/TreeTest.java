package dev.tale.cli.repository2.object;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class TreeTest {
    @Test
    public void serializeTree() {
        Tree.TreeBuilder builder = Tree.builder();
        builder.addChild(createEntry("185f8db32271fe25f561a6fc938b2e264306ec304eda518007d1764826381969", "Hello.java", ObjectType.BLOB));
        builder.addChild(createEntry("25a6634263c1b1f6fc4697a04e2b9904ea4b042a89af59dc93ec1f5d44848a26", "src", ObjectType.TREE));

        Tree expected = builder.build();
        byte[] serialized = expected.serialize();

        Tree actual = new Tree().deserialize(serialized);

        assertThat(actual, is(equalTo(expected)));
    }

    @Test
    public void serializeEmptyTree() {
        Tree.TreeBuilder builder = Tree.builder();
        Tree expected = builder.build();
        byte[] serialized = expected.serialize();

        Tree actual = new Tree().deserialize(serialized);

        assertThat(actual, is(equalTo(expected)));
    }

    private Tree.Entry createEntry(String object, String name, ObjectType type) {
        Tree.Entry entry = new Tree.Entry();
        entry.setObjectName(object.getBytes());
        entry.setName(name.getBytes());
        entry.setType(type.prefix);
        return entry;
    }
}
