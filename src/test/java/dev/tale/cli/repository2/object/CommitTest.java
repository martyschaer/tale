package dev.tale.cli.repository2.object;

import dev.tale.cli.exceptions.ApplicationException;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class CommitTest {
    private static final String hash1 = "a7e64b1d8f42e11ca5e984d673adb0703a164b0872d12eb2a6004616abb2b2dd";
    private static final String hash2 = "c8fe5d507f207a382123d83514cfc112fdf25d7f2475d37cda0efddbd730db37";
    private static final String hash3 = "dc9c5edb8b2d479e697b4b0b8ab874f32b325138598ce9e7b759eb8292110622";

    @Test
    public void serializeMergeCommit() {
        Commit expected = new Commit();
        expected.setAuthor("author");
        expected.setMessage("Initial Commit\nPair: coauth");
        expected.setParent1(hash1);
        expected.setParent2(hash2);
        expected.setTimestamp(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        expected.setTree(hash3);

        byte[] serialized = expected.serialize();

        Commit actual = new Commit().deserialize(serialized);

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void serializeSingleParentCommit() {
        Commit expected = new Commit();
        expected.setAuthor("author");
        expected.setMessage("Initial Commit\nPair: coauth");
        expected.setParent1(hash1);
        expected.setTimestamp(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        expected.setTree(hash3);

        byte[] serialized = expected.serialize();

        Commit actual = new Commit().deserialize(serialized);

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void serializeOrphanCommit() {
        Commit expected = new Commit();
        expected.setAuthor("author");
        expected.setMessage("Initial Commit\nPair: coauth");
        expected.setTimestamp(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        expected.setTree(hash3);

        byte[] serialized = expected.serialize();

        Commit actual = new Commit().deserialize(serialized);

        assertThat(actual, equalTo(expected));
    }

    @Test
    public void serializeNoMessageCommit() {
        Commit expected = new Commit();
        expected.setAuthor("author");
        expected.setMessage("");
        expected.setTimestamp(LocalDateTime.now().toEpochSecond(ZoneOffset.UTC));
        expected.setTree(hash3);

        ApplicationException exception = assertThrows(ApplicationException.class, () -> {
            byte[] serialized = expected.serialize();
        });
        assertThat(exception.getMessage(), containsString("message must be provided"));
    }
}
