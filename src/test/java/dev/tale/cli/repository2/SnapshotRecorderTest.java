package dev.tale.cli.repository2;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.function.FileService;
import dev.tale.cli.repository2.index.Index2;
import dev.tale.cli.repository2.index.IndexEntry;
import dev.tale.cli.repository2.index.Stage;
import dev.tale.cli.repository2.object.ObjectType;
import dev.tale.cli.repository2.object.Tree;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SnapshotRecorderTest {
    private static final String readmeHash = "a7e64b1d8f42e11ca5e984d673adb0703a164b0872d12eb2a6004616abb2b2dd";
    private static final String pdfHash = "c8fe5d507f207a382123d83514cfc112fdf25d7f2475d37cda0efddbd730db37";
    private static final String javaHash = "dc9c5edb8b2d479e697b4b0b8ab874f32b325138598ce9e7b759eb8292110622";
    private static final String docTreeHash = "8b38705a95b19c25b12957582e1319f35b379c851f11da5dd7f8586e57c91dbb";
    private static final String srcTreeHash = "cd11c3cc19450bd8329acd290a5c6a88bb84f2ee3c83d1ab60e9e19e95842177";
    private static final String mainTreeHash = "96faa8454c4dd3d0d955afbebc0d78f93cba47a873888105744b5c0742aa78d7";

    private static final String readmeName = "README.md";
    private static final String pdfName = "document.pdf";
    private static final String javaName = "Hello.java";

    private static final IndexEntry readmeEntry = new IndexEntry(readmeName, Stage.NORMAL, readmeHash);
    private static final IndexEntry pdfEntry = new IndexEntry("doc/" + pdfName, Stage.NORMAL, pdfHash);
    private static final IndexEntry javaEntry = new IndexEntry("src/main/" + javaName, Stage.NORMAL, javaHash);

    private SnapshotRecorder recorder;
    private ObjectReader reader;

    private static FileService fileService;
    private static Index2 index;

    @BeforeAll
    public static void setupMock() {
        fileService = new TestFileService();
        ServiceFactory.putMock(FileService.class, fileService);
    }

    @BeforeEach
    public void setup() {
        index = mock(Index2.class);
        when(index.getInstance()).thenReturn(index);
        ServiceFactory.putMock(Index2.class, index);

        recorder = ServiceFactory.get(SnapshotRecorder.class);
        reader = ServiceFactory.get(ObjectReader.class);
    }

    @Test
    public void testOnlyTopLevelFiles() {
        when(index.getAllEntries()).thenReturn(Collections.singletonList(readmeEntry));

        String object = recorder.recordState();

        Tree actual = (Tree) reader.readObject(object);
        Tree.Entry expected = new Tree.Entry(ObjectType.BLOB, readmeEntry.getObject(), readmeName);

        assertThat(actual.getChildren(), containsInAnyOrder(expected));
    }

    @Test
    public void testTwoLevels() {
        when(index.getAllEntries()).thenReturn(Arrays.asList(readmeEntry, pdfEntry));

        String object = recorder.recordState();

        Tree actual = (Tree) reader.readObject(object);

        Tree.Entry expectedReadmeBlob = new Tree.Entry(ObjectType.BLOB, readmeEntry.getObject(), readmeName);
        Tree.Entry expectedSubTree = new Tree.Entry(ObjectType.TREE, docTreeHash, "doc");

        assertThat(actual.getChildren(), containsInAnyOrder(expectedReadmeBlob, expectedSubTree));

        actual = (Tree) reader.readObject(docTreeHash);

        Tree.Entry expectedPdfBlob = new Tree.Entry(ObjectType.BLOB, pdfEntry.getObject(), pdfName);

        assertThat(actual.getChildren(), containsInAnyOrder(expectedPdfBlob));
    }

    @Test
    public void testThreeLevels() {
        when(index.getAllEntries()).thenReturn(Arrays.asList(readmeEntry, pdfEntry, javaEntry));

        String object = recorder.recordState();

        // assert root
        Tree actual = (Tree) reader.readObject(object);
        Tree.Entry expectedReadmeBlob = new Tree.Entry(ObjectType.BLOB, readmeEntry.getObject(), readmeName);
        Tree.Entry expectedDocSubTree = new Tree.Entry(ObjectType.TREE, docTreeHash, "doc");
        Tree.Entry expectedSrcSubTree = new Tree.Entry(ObjectType.TREE, srcTreeHash, "src");
        assertThat(actual.getChildren(), containsInAnyOrder(expectedReadmeBlob, expectedDocSubTree, expectedSrcSubTree));

        // assert doc
        actual = (Tree) reader.readObject(docTreeHash);
        Tree.Entry expectedPdfBlob = new Tree.Entry(ObjectType.BLOB, pdfEntry.getObject(), pdfName);
        assertThat(actual.getChildren(), containsInAnyOrder(expectedPdfBlob));

        // assert src
        actual = (Tree)reader.readObject(srcTreeHash);
        Tree.Entry expectedMainTree = new Tree.Entry(ObjectType.TREE, mainTreeHash, "main");
        assertThat(actual.getChildren(), containsInAnyOrder(expectedMainTree));

        // assert src/main
        actual = (Tree)reader.readObject(mainTreeHash);
        Tree.Entry expectedJavaBlob = new Tree.Entry(ObjectType.BLOB, javaHash, javaName);
        assertThat(actual.getChildren(), containsInAnyOrder(expectedJavaBlob));
    }
}