package dev.tale.cli.repository2;

import dev.tale.cli.function.FileService;

import java.nio.file.Path;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Doesn't write files to disk, but stores them in a Map.
 */
public class TestFileService extends FileService {
    private Map<Path, byte[]> files = new ConcurrentHashMap<>();

    @Override
    public void write(Path path, byte[] content) {
        files.put(path, content);
    }

    @Override
    public byte[] load(Path path) {
        if(!files.containsKey(path)) {
            System.out.println("Couldn't find " + path.toString());
            for(Path p : files.keySet()) {
                System.out.println(" _" + p.toString());
            }
            throw new IllegalArgumentException(path.toString() + " doesn't exist.");
        }
        return files.get(path);
    }
}
