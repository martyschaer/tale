package dev.tale.cli.patch;

import dev.tale.cli.BaseTestCase;
import dev.tale.cli.util.FileUtil;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;

public class DifferenceAlgorithmTest extends BaseTestCase {

    @Test
    public void testLaoTzu() throws Exception {
        assertFileDiff("diff/lao", "diff/tzu");
    }

    @Test
    public void testLorem() throws Exception {
        assertFileDiff("diff/lorem_a.txt", "diff/lorem_b.txt");
    }

    @Test
    public void testTzu_Tzu2() throws Exception {
        assertFileDiff("diff/tzu", "diff/tzu_2");
    }

    @Test
    public void testJavaDiff() throws Exception {
        assertFileDiff("diff/a.java", "diff/b.java");
    }

    @Test
    public void testPHPDiff() throws Exception {
        assertFileDiff("diff/a.php", "diff/b.php");
    }

    @Test
    public void testApplicationDiff() throws Exception {
        assertFileDiff("diff/a_app.java", "diff/b_app.java");
    }

    @Test
    public void testNewFileDiff() throws Exception {
        assertDiff(new ArrayList<>(), FileUtil.loadResource("diff/lao"));
    }

    @Test
    public void testDeleteDiff() throws Exception {
        assertDiff(FileUtil.loadResource("diff/tzu"), new ArrayList<>());
    }

    private void assertFileDiff(String aName, String bName) throws Exception {
        List<String> a = FileUtil.loadResource(aName);
        List<String> b = FileUtil.loadResource(bName);

        assertDiff(a, b);
    }

    private void assertDiff(List<String> a, List<String> b) {
        DifferenceAlgorithm differ = new DifferenceAlgorithm(a, b);
        Patch difference = PatchGenerator.generate(differ.run(), a, b);

        Patcher patcher = new Patcher(difference.getHunks());

        patcher.applyTo(a);

        assertThat(b, is(equalTo(a)));
    }
}
