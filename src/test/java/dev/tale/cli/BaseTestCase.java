package dev.tale.cli;

import dev.tale.cli.implementation.Initializer;
import dev.tale.cli.util.FileUtil;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.nio.file.Paths;

abstract public class BaseTestCase {
    protected final ByteArrayOutputStream systemOut = new ByteArrayOutputStream();
    protected final ByteArrayOutputStream systemErr = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(systemOut));
        System.setErr(new PrintStream(systemErr));

        new Initializer().initialize();
    }

    protected boolean removeTaleFolder() {
        return FileUtil.remove(Paths.get(".tale").toFile());
    }

    @AfterEach
    public void tearDown() {
        this.removeTaleFolder();
    }
}
