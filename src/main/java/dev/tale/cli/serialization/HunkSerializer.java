package dev.tale.cli.serialization;

import com.google.gson.*;
import dev.tale.cli.patch.Hunk;
import dev.tale.cli.patch.HunkHeader;

import java.lang.reflect.Type;

public class HunkSerializer implements JsonSerializer<Hunk>, JsonDeserializer<Hunk> {
    private static final String HEADER = "header";
    private static final String CONTENT = "content";
    @Override
    public Hunk deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonElement headerElement = jsonElement.getAsJsonObject().get(HEADER);
        HunkHeader hunkHeader = context.deserialize(headerElement, HunkHeader.class);

        Hunk hunk = new Hunk(hunkHeader);

        jsonElement.getAsJsonObject().getAsJsonArray(CONTENT).forEach(je -> hunk.addContent(je.getAsString()));

        return hunk;
    }

    @Override
    public JsonElement serialize(Hunk hunk, Type type, JsonSerializationContext context) {
        JsonObject json = new JsonObject();
        json.add(HEADER, context.serialize(hunk.getHeader(), HunkHeader.class));
        JsonArray jsonHunkContent = new JsonArray(hunk.getContent().size());

        hunk.getContent().forEach(jsonHunkContent::add);

        json.add(CONTENT, jsonHunkContent);
        return json;
    }
}
