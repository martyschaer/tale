package dev.tale.cli;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.command.Tale;
import dev.tale.cli.exceptions.ApplicationException;
import picocli.CommandLine;

public class Application {
    public static void main(String[] args) {
        try {
            CommandLine commandLine = new CommandLine(new Tale());

            commandLine.setExecutionExceptionHandler((exception, cmd, parseResult) -> {
                if(exception instanceof ApplicationException) {
                    System.err.println(exception.getMessage());
                    System.exit(ExitCode.ERROR);
                }
                throw exception;
            });

            commandLine.setColorScheme(CommandLine.Help.defaultColorScheme(CommandLine.Help.Ansi.ON));

            try {
                commandLine.parseArgs(args);
            } catch (CommandLine.UnmatchedArgumentException | CommandLine.MissingParameterException exception) {
                System.out.println(exception.getMessage());
                commandLine.usage(System.out);
                System.exit(ExitCode.USAGE);
            }

            if (commandLine.isUsageHelpRequested()) {
                commandLine.usage(System.out);
                System.exit(ExitCode.USAGE);
            }
            if (commandLine.isVersionHelpRequested()) {
                commandLine.printVersionHelp(System.out);
                System.exit(ExitCode.USAGE);
            }


            int exitCode = commandLine.execute(args);
            System.exit(exitCode);
        } catch (ApplicationException e) {
            System.err.println(e.getMessage());
            System.exit(ExitCode.ERROR);
        }
    }
}
