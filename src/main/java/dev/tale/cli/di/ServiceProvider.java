package dev.tale.cli.di;

/**
 * Used for {@link Inject}able services.
 * @param <T> - the implementing class
 */
public interface ServiceProvider<T> {
    /**
     * Run after initialization.
     */
    void init();

    /**
     * Should return the instance of the implementing class.
     * @return this;
     */
    T getInstance();

    /**
     * @return - Class[] of the services requested by using {@link Inject}.
     */
    Class<?>[] getDependencies();
}
