package dev.tale.cli.di;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Can be used to have implementations of {@link ServiceProvider} injected.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
}
