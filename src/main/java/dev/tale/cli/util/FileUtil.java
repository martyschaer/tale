package dev.tale.cli.util;

import dev.tale.cli.exceptions.ApplicationException;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Encapsulates common file system operations:
 * - finding files in the .tale-directory
 * - finding changed files
 * - writing to files
 * - removing files
 * - newline-sanatization
 */
public class FileUtil {
    private static final String EOL_REGEX = "\\R";
    private static final String DOT_TALE = ".tale";

    public static Path findDotTale(Path parent) {
        File dotTale = findDotTale(parent.toFile());
        if (dotTale == null) {
            return null;
        }
        return dotTale.toPath();
    }

    public static File findDotTale(File parent) {
        if (parent.isDirectory()) {
            String[] result = parent.list((current, name) -> name.equals(DOT_TALE) && new File(current, name).isDirectory());
            if (result != null && result.length == 1) {
                return new File(parent, result[0]);
            }
        }

        if (parent.getParent() == null) {
            return null;
        } else {
            return findDotTale(parent.toPath().getParent().toFile());
        }
    }

    public static Path findFileInDotTale(String... path) {
        RepositoryUtil.assertRunningInRepository();

        return Paths.get(PathUtil.getInstance().getDotTale().toAbsolutePath().toString(), path);
    }

    public static List<Path> findFilesChangedSince(Path findIn, long timestamp) {
        return findFilesRecursively(findIn, file -> file.lastModified() >= timestamp);
    }

    public static List<Path> findFilesRecursively(Path findIn) {
        return findFilesRecursively(findIn, null);
    }

    public static List<Path> findFilesRecursively(Path findIn, FileFilter filter) {
        File findInFile = findIn.toFile();
        if (findInFile.isFile() && (filter == null || filter.accept(findInFile))) {
            return Collections.singletonList(findIn);
        } else if (findInFile.isDirectory() && !findInFile.getName().equals(DOT_TALE)) {
            File[] files = Objects.requireNonNull(findInFile.listFiles(filter));
            return Arrays.stream(files) //
                    .map(File::toPath) //
                    .map(path -> findFilesRecursively(path, filter)) //
                    .flatMap(List::stream) //
                    .map(PathUtil::relativize) //
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    public static void writeContentToPath(Path path, String content) {
        writeContentToPath(path, Collections.singletonList(content));
    }

    public static boolean remove(File f) {
        boolean successful = true;
        if (f.isDirectory()) {
            successful = Arrays.stream(f.list()).allMatch(c -> remove(new File(f, c)));
        }
        successful &= f.delete();
        return successful;
    }

    public static void writeContentToPath(Path path, List<String> content) {
        StringBuilder sb = new StringBuilder();
        content.stream().map(FileUtil::unsanitize).forEach(sb::append);
        writeContentToPath(path, sb.toString().getBytes());
    }

    public static void writeContentToPath(Path path, byte[] content) {
        try {
            File f = path.toAbsolutePath().toFile();
            if (!path.toAbsolutePath().getParent().toFile().exists()) {
                Files.createDirectories(path.getParent());
            }

            if (!f.exists() && !f.createNewFile()) {
                throw new IllegalStateException("Couldn't create a new file at: " + path.toAbsolutePath());
            }

            if (!f.canWrite() && !f.setWritable(true)) {
                throw new IllegalStateException("Couldn't set writeable=true on: " + path.toAbsolutePath());
            }
            Files.write(path, content);
        } catch (IOException e) {
            throw new ApplicationException("Couldn't write file to disk", e);
        }
    }

    public static byte[] loadFileFromPathAsBytes(Path path) {
        try {
            return Files.readAllBytes(path);
        } catch (IOException e) {
            throw new ApplicationException("Couldn't read path: " + path, e);
        }
    }

    public static List<String> loadFileFromPath(Path path) {
        String content = new String(loadFileFromPathAsBytes(path));
        return toLines(content);
    }

    private static List<String> toLines(String content) {
        List<String> lines = new ArrayList<>();
        String[] rawLines = content.split(EOL_REGEX);

        // find all line breaks (\r\n, \n, etc..) and match them to their corresponding line
        Matcher m = Pattern.compile(EOL_REGEX).matcher(content);
        for (String line : rawLines) {
            lines.add(line + (m.find() ? sanatize(m.group()) : ""));
        }

        return lines;
    }

    public static String sanatize(String input) {
        return input.replace("\n", "\\n").replace("\r", "\\r");
    }

    public static String unsanitize(String input) {
        return input.replace("\\n", "\n").replace("\\r", "\r");
    }

    public static List<String> loadResource(String name) {
        return loadFileFromPath(getResourcePath(name));
    }

    public static Path getResourcePath(String name) {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        return Paths.get(new File(classLoader.getResource(name).getFile()).getAbsolutePath());
    }
}
