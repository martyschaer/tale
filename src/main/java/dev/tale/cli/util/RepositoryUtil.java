package dev.tale.cli.util;

import dev.tale.cli.exceptions.NotInRepositoryException;

/**
 * Used to assert whether were currently running in a tale repository.
 */
public class RepositoryUtil {
    /**
     * Prints an error message and exits when not running in a tale repository.
     */
    public static void assertRunningInRepository() {
        if (PathUtil.getInstance().getDotTale()== null || !PathUtil.getInstance().getDotTale().toFile().exists()) {
            throw new NotInRepositoryException("Not running in a tale repository.");
        }
    }
}
