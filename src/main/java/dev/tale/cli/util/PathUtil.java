package dev.tale.cli.util;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;

/**
 * Converts paths to be relative to the repository root
 * and holds some useful constants.
 */
public class PathUtil {
    private static PathUtil instance;

    public static PathUtil getInstance() {
        if(instance == null) {
            instance = new PathUtil();
        }
        return instance;
    }

    private final Path workingDir;
    private final Path dotTale;
    private final Path root;

    private PathUtil() {
        this.workingDir = Paths.get("").toAbsolutePath();
        this.dotTale = FileUtil.findDotTale(workingDir);
        this.root = Objects.nonNull(dotTale) ? dotTale.getParent() : workingDir;
    }

    public Path getWorkingDir() {
        return this.workingDir;
    }

    public Path getDotTale() {
        return this.dotTale;
    }

    public Path getRoot() {
        return this.root;
    }

    public static final Path DEV_NULL = Paths.get("/dev/null");

    public static Path relativize(Path input) {
        if(input.equals(DEV_NULL)) {
            return input;
        }
        return getInstance().root.relativize(input.toAbsolutePath());
    }

    public static boolean isNullEquivalent(Path path) {
        return path == null || path.equals(DEV_NULL);
    }
}
