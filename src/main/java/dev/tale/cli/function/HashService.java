package dev.tale.cli.function;

import dev.tale.cli.di.ServiceProvider;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class HashService implements ServiceProvider<HashService> {

    public static final int DIGEST_LENGTH = 64;
    private static final String DIGEST_ALGORITHM = "SHA-256";
    private MessageDigest digest;

    @Override
    public void init() {
        try{
            this.digest = MessageDigest.getInstance(DIGEST_ALGORITHM);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("Couldn't initialize HashService, because the system lacks support for SHA-256");
        }
    }

    @Override
    public HashService getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[0];
    }

    public byte[] hash(byte[] input) {
        return digest.digest(input);
    }

}
