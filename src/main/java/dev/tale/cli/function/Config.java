package dev.tale.cli.function;

import dev.tale.cli.di.ServiceProvider;

public class Config implements ServiceProvider<Config> {
    private String user = null;

    @Override
    public void init() {

    }

    @Override
    public Config getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[0];
    }

    public String getUser() {
        if(user == null) {
            user = System.getProperty("user.name");
            if (user == null) {
                user = "unknown";
            }
        }
        return user;
    }
}
