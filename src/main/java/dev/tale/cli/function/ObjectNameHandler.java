package dev.tale.cli.function;

import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.exceptions.ApplicationException;

import java.nio.file.Path;
import java.util.List;

import static dev.tale.cli.function.HashService.DIGEST_LENGTH;

public class ObjectNameHandler implements ServiceProvider<ObjectNameHandler> {

    private static final int BUCKET_NAME_LENGTH = 2; // number of nibbles (hex-chars) used to name a bucket

    @Inject
    FileService fileService;

    @Inject
    PathService pathService;

    @Override
    public void init() {
        // NOP
    }

    @Override
    public ObjectNameHandler getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{FileService.class, PathService.class};
    }

    public Path toPath(String objectName) {
        String resolved = resolvePartialObjectName(objectName);
        return pathService.inObjectDir(
                resolved.substring(0, BUCKET_NAME_LENGTH),
                resolved.substring(BUCKET_NAME_LENGTH));
    }

    private String resolvePartialObjectName(String partial) {
        // given partial is already a full object name
        if (partial.length() == DIGEST_LENGTH) {
            return partial;
        }

        if (partial.length() < 4) {
            throw new ApplicationException("Need at least 4 characters of the object name.");
        }

        String bucket = partial.substring(0, BUCKET_NAME_LENGTH);

        List<String> candidates = fileService.listChildren(pathService.inObjectDir(bucket));

        if (candidates.size() == 1) {
            return bucket + candidates.get(0);
        }

        if (candidates.size() == 0) {
            throw new ApplicationException(partial + " is not an object name");
        }

        throw new ApplicationException("Multiple candidates for '" + partial + "'. Try entering more characters.");
    }
}
