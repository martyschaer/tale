package dev.tale.cli.function;

import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.exceptions.ApplicationException;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FileService implements ServiceProvider<FileService> {

    @Override
    public void init() {
        // NOP
    }

    @Override
    public FileService getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[0];
    }

    public void write(Path path, byte[] content) {
        try {
            File f = path.toAbsolutePath().toFile();
            if (!path.toAbsolutePath().getParent().toFile().exists()) {
                Files.createDirectories(path.getParent());
            }

            if (!f.exists() && !f.createNewFile()) {
                throw new IllegalStateException("Couldn't create a new file at: " + path.toAbsolutePath());
            }

            if (!f.canWrite() && !f.setWritable(true)) {
                throw new IllegalStateException("Couldn't set writeable=true on: " + path.toAbsolutePath());
            }
            Files.write(path, content);
        } catch (IOException e) {
            throw new ApplicationException("Couldn't write file to disk", e);
        }
    }

    public String loadAsString(Path path) {
        return new String(load(path));
    }

    public byte[] load(Path path) {
        try {
            return Files.readAllBytes(path);
        } catch (IOException e) {
            throw new ApplicationException("Couldn't read path: " + path, e);
        }
    }

    public List<String> listChildren(Path parent) {
        File[] children = parent.toFile().listFiles();
        if(children == null) {
            return Collections.emptyList();
        }
        return Arrays.stream(children) //
            .filter(File::isFile) //
            .map(File::getName) //
            .collect(Collectors.toList());
    }

    public boolean exists(Path path) {
        return Files.exists(path, LinkOption.NOFOLLOW_LINKS);
    }
}
