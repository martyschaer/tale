package dev.tale.cli.function;

import dev.tale.cli.di.ServiceProvider;

public class HexEncoder implements ServiceProvider<HexEncoder> {
    private static final char[] HEX_DIGITS = "0123456789abcdef".toCharArray();

    @Override
    public void init() {
        //NOP
    }

    @Override
    public HexEncoder getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[0];
    }

    public String toHexString(byte[] input) {
        char[] output = new char[input.length * 2];
        for(int i = 0; i < input.length; i++) {
            int value = input[i] & 0xFF;
            output[i * 2] = HEX_DIGITS[value >>> 4];
            output[i * 2 + 1] = HEX_DIGITS[value & 0x0F];
        }
        return new String(output);
    }
}
