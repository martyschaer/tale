package dev.tale.cli.function;

import dev.tale.cli.di.ServiceProvider;

import java.nio.file.Path;
import java.nio.file.Paths;

public class PathService implements ServiceProvider<PathService> {
    private static final String DOT_TALE = ".tale";

    public static final Path TALE_DIR = Paths.get(DOT_TALE);
    public static final Path HEAD = Paths.get(DOT_TALE, "HEAD");
    public static final Path OBJECT_DIR = Paths.get(DOT_TALE, "objects");

    @Override
    public void init() {
    }

    @Override
    public PathService getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[0];
    }

    public Path inDotTale(String first, String... more) {
        return inDotTale(Paths.get(first, more));
    }

    public Path inDotTale(Path path) {
        return TALE_DIR.resolve(path);
    }

    public Path inObjectDir(String bucket, String... more) {
        return OBJECT_DIR.resolve(Paths.get(bucket, more));
    }
}
