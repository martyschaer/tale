package dev.tale.cli.function;

import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.exceptions.ApplicationException;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static dev.tale.cli.function.HashService.DIGEST_LENGTH;

public class BranchNameHandler implements ServiceProvider<BranchNameHandler> {

    @Inject
    FileService fileService;

    @Inject
    PathService pathService;

    @Override
    public void init() {
        // NOP
    }

    @Override
    public BranchNameHandler getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{FileService.class, PathService.class};
    }

    /**
     * Returns commit ID of the current HEAD.
     */
    public String getHEAD() {
        return fileService.loadAsString(getCurrentBranchPath());
    }

    /**
     * Writes the given commit ID to the HEAD.
     */
    public void putHEAD(String commitID) {
        fileService.write(PathService.HEAD, commitID.getBytes());
    }

    /**
     * Returns the current branch name.
     */
    public String getCurrentBranch() {
        return fileService.loadAsString(PathService.HEAD);
    }

    private Path getCurrentBranchPath() {
        return pathService.inDotTale(getCurrentBranch());
    }
}
