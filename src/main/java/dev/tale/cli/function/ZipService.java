package dev.tale.cli.function;

import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.exceptions.ApplicationException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class ZipService implements ServiceProvider<ZipService> {
    @Override
    public void init() {
    }

    @Override
    public ZipService getInstance() {
        return null;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[0];
    }

    public byte[] deflate(byte[] input) {
        Deflater deflater = new Deflater();
        deflater.setInput(input);

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        deflater.finish();
        byte[] buf = new byte[1024];
        while(!deflater.finished()) {
            int len = deflater.deflate(buf);
            baos.write(buf, 0, len);
        }
        deflater.end();
        return baos.toByteArray();
    }

    public byte[] inflate(byte[] input) {
        try{
            Inflater inflater = new Inflater();
            inflater.setInput(input);

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            while(!inflater.finished()) {
                int len = inflater.inflate(buf);
                baos.write(buf, 0, len);
            }
            baos.close();

            byte[] output = baos.toByteArray();

            inflater.end();
            return output;
        }catch (DataFormatException | IOException e) {
            throw new ApplicationException("Couldn't inflate.", e);
        }
    }
}
