package dev.tale.cli.repository2.serialization;

import java.util.Arrays;
import java.util.Spliterator;
import java.util.function.Consumer;

public class ByteArraySpliterator implements Spliterator<byte[]> {

    private final byte[] array;
    private final byte separator;
    private int lastOffset;

    public ByteArraySpliterator(byte[] array, byte separator) {
        this.array = array;
        this.separator = separator;
    }

    @Override
    public boolean tryAdvance(Consumer<? super byte[]> consumer) {
        if(estimateSize() > 1) {
            int offset = lastOffset;
            while (array[++offset] != separator) {}
            byte[] bytes = Arrays.copyOfRange(array, lastOffset, offset);
            consumer.accept(bytes);
            lastOffset = offset + 1;
            return true;
        }
        return false;
    }

    @Override
    public Spliterator<byte[]> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return array.length - lastOffset;
    }

    @Override
    public int characteristics() {
        return ORDERED | SIZED | SUBSIZED | IMMUTABLE;
    }
}
