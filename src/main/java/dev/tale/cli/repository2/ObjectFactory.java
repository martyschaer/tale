package dev.tale.cli.repository2;

import dev.tale.cli.di.ServiceProvider;

public class ObjectFactory implements ServiceProvider<ObjectFactory> {
    @Override
    public void init() {

    }

    @Override
    public ObjectFactory getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[0];
    }
}
