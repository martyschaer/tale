package dev.tale.cli.repository2.object;


import dev.tale.cli.exceptions.ApplicationException;
import dev.tale.cli.repository2.serialization.ByteArraySpliterator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.LinkedList;
import java.util.Objects;

import static dev.tale.cli.repository2.serialization.SerializationHelper.NULL_BYTE;

/**
 * Represents a commit on disk.
 * References a {@link Tree} (the snapshot of the repository),
 * its two parent commits (usually one, but in case of merges two),
 * a timestamp of its creation,
 * and the commit message.
 */
@Builder
@AllArgsConstructor
@Data
public class Commit extends TaleObject {
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);

    private String tree;
    private String parent1;
    private String parent2;
    private String author;
    private Long timestamp;
    private String message;

    public Commit() {
        super();
    }

    @Override
    public byte[] serialize() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            // type prefix
            output.write(ObjectType.COMMIT.prefix);
            output.write(NULL_BYTE);

            output.write(tree.getBytes());
            output.write(NULL_BYTE);
            if (Objects.nonNull(parent1)) {
                output.write(parent1.getBytes());
                output.write(NULL_BYTE);
            }
            if (Objects.nonNull(parent2)) {
                output.write(parent2.getBytes());
                output.write(NULL_BYTE);
            }
            output.write(author.getBytes());
            output.write(NULL_BYTE);

            output.write(timestamp.toString().getBytes());
            output.write(NULL_BYTE);

            if (message == null || message.isEmpty()) {
                throw new ApplicationException("A message must be provided.");
            }

            output.write(message.getBytes());
            output.write(NULL_BYTE);
        } catch (IOException e) {
            throw new ApplicationException("Couldn't serialize commit object", e);
        }
        return output.toByteArray();
    }

    // TODO potentially create a TimeService to deal with timezones etc.
    public void setTimestampToNow() {
        this.timestamp = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
    }

    public String getFormattedTimestamp() {
        return LocalDateTime.ofEpochSecond(this.timestamp, 0, ZoneOffset.UTC).format(FORMATTER);
    }

    @Override
    public ObjectType getType() {
        return ObjectType.COMMIT;
    }

    @Override
    public String toCli() {
        StringBuilder builder = new StringBuilder();
        builder.append("tree\t").append(this.tree).append("\n");
        if (this.parent1 != null) {
            builder.append("parent\t").append(this.parent1).append("\n");
        }
        if (this.parent2 != null) {
            builder.append("parent\t").append(this.parent2).append("\n");
        }
        builder.append("author\t").append(this.author).append(" ") //
                .append(getFormattedTimestamp()) //
                .append("\n");
        builder.append("\n").append(this.message).append("\n");
        return builder.toString();
    }

    @Override
    public Commit deserialize(byte[] serialized) {
        ByteArraySpliterator spliterator = new ByteArraySpliterator(serialized, NULL_BYTE);

        // skip the type prefix
        spliterator.tryAdvance(unused -> {
        });

        LinkedList<String> data = new LinkedList<>();
        while (spliterator.tryAdvance(bytes -> data.add(new String(bytes)))) {
        }

        CommitBuilder builder = builder();
        builder.tree(data.removeFirst());
        builder.message(data.removeLast());
        builder.timestamp(Long.parseLong(data.removeLast()));
        builder.author(data.removeLast());

        if (data.size() > 0) {
            builder.parent1(data.removeFirst());
        }

        if (data.size() > 0) {
            builder.parent2(data.removeFirst());
        }

        return builder.build();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Commit commit = (Commit) o;

        if (!Objects.equals(tree, commit.tree)) return false;
        if (!Objects.equals(parent1, commit.parent1)) return false;
        if (!Objects.equals(parent2, commit.parent2)) return false;
        if (!Objects.equals(author, commit.author)) return false;
        if (timestamp != null ? !timestamp.equals(commit.timestamp) : commit.timestamp != null) return false;
        return message != null ? message.equals(commit.message) : commit.message == null;
    }

    @Override
    public int hashCode() {
        int result = tree != null ? tree.hashCode() : 0;
        result = 31 * result + (parent1 != null ? parent1.hashCode() : 0);
        result = 31 * result + (parent2 != null ? parent2.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (timestamp != null ? timestamp.hashCode() : 0);
        result = 31 * result + (message != null ? message.hashCode() : 0);
        return result;
    }
}
