package dev.tale.cli.repository2.object;

import dev.tale.cli.exceptions.ApplicationException;
import dev.tale.cli.repository2.serialization.ByteArraySpliterator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static dev.tale.cli.repository2.serialization.SerializationHelper.NULL_BYTE;

/**
 * Represents a {@link Tree} on disk.
 * References the other {@link Tree}s and {@link Blob}s below it.
 */
@Builder
@AllArgsConstructor
@Data
public class Tree extends TaleObject {
    Set<Entry> children = new HashSet<>();

    public Tree() {
        super();
    }

    @Override
    public byte[] serialize() {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            output.write(getType().prefix);
            output.write(NULL_BYTE);

            for (Entry child : children) {
                output.write(child.getType().prefix);
                output.write(NULL_BYTE);
                output.write(child.getObjectName().getBytes());
                output.write(NULL_BYTE);
                output.write(child.getName().getBytes());
                output.write(NULL_BYTE);
            }
        } catch (IOException e) {
            throw new ApplicationException("Could not serialize object.");
        }

        return output.toByteArray();
    }

    @Override
    public ObjectType getType() {
        return ObjectType.TREE;
    }

    @Override
    public String toCli() {
        return children.stream() //
                .sorted(Comparator.comparing(Entry::getName)) //
                .map(Entry::toString) //
                .collect(Collectors.joining("\n"));
    }

    @Override
    public Tree deserialize(byte[] serialized) {
        TreeBuilder builder = builder();

        Spliterator<byte[]> spliterator = new ByteArraySpliterator(serialized, NULL_BYTE);

        // advance once to get rid of the ObjectType prefix
        spliterator.tryAdvance(unused -> {
        });

        Entry childEntry = new Entry();

        while (spliterator.tryAdvance(childEntry::setType)) {
            if (!spliterator.tryAdvance(childEntry::setObjectName)) {
                throw new ApplicationException("Couldn't deserialize. Invalid format.");
            }

            if (!spliterator.tryAdvance(childEntry::setName)) {
                throw new ApplicationException("Couldn't deserialize. Invalid format.");
            }

            builder.addChild(childEntry);
            childEntry = new Entry(); // setup the next instance
        }

        return builder.build();
    }

    public static class Entry {
        private ObjectType type;
        private String objectName;
        private String name;

        public Entry() {

        }

        public Entry(ObjectType type, String objectName, String name) {
            this.type = type;
            this.objectName = objectName;
            this.name = name;
        }

        public void setType(byte[] bytes) {
            type = ObjectType.getType(bytes);
        }

        public void setObjectName(byte[] bytes) {
            objectName = new String(bytes);
        }

        public void setName(byte[] bytes) {
            name = new String(bytes);
        }

        public ObjectType getType() {
            return type;
        }

        public String getObjectName() {
            return objectName;
        }

        public String getName() {
            return name;
        }

        public String toString() {
            StringBuilder builder = new StringBuilder();

            builder.append(getType().strPrefix);
            builder.append("\t");
            builder.append(getObjectName());
            builder.append("\t");
            builder.append(getName());

            return builder.toString();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Entry entry = (Entry) o;

            if (type != entry.type) return false;
            if (!Objects.equals(objectName, entry.objectName)) return false;
            return Objects.equals(name, entry.name);
        }

        @Override
        public int hashCode() {
            int result = type != null ? type.hashCode() : 0;
            result = 31 * result + (objectName != null ? objectName.hashCode() : 0);
            result = 31 * result + (name != null ? name.hashCode() : 0);
            return result;
        }
    }

    public static class TreeBuilder {
        private Set<Entry> children = new HashSet<>();

        public TreeBuilder addChild(Entry child) {
            children.add(child);
            return this;
        }

        public TreeBuilder addChild(String objectName, String name, ObjectType type) {
            children.add(new Entry(type, objectName, name));
            return this;
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tree tree = (Tree) o;

        return Objects.equals(children, tree.children);
    }

    @Override
    public int hashCode() {
        return children != null ? children.hashCode() : 0;
    }
}
