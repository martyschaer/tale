package dev.tale.cli.repository2.object;

import dev.tale.cli.exceptions.ApplicationException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

/**
 * Represents a snapshot of a file on disk.
 */
@Builder
@AllArgsConstructor
@Data
public class Blob extends TaleObject {
    private byte[] content = new byte[0];

    public Blob() {
        super();
    }

    @Override
    public byte[] serialize() {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream(content.length + ObjectType.BLOB.prefix.length);
            outputStream.write(ObjectType.BLOB.prefix);
            outputStream.write(content);
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw new ApplicationException("Couldn't serialize blob", e);
        }
    }

    @Override
    public ObjectType getType() {
        return ObjectType.BLOB;
    }

    @Override
    public String toCli() {
        return new String(content);
    }

    @Override
    public Blob deserialize(byte[] serialized) {
        Blob blob = new Blob();
        blob.content = Arrays.copyOfRange(serialized, ObjectType.BLOB.prefix.length, serialized.length);
        return blob;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Blob blob = (Blob) o;

        return Arrays.equals(content, blob.content);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(content);
    }
}
