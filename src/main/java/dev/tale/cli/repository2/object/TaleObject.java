package dev.tale.cli.repository2.object;

/**
 * Takes care of serialization and deserialization,
 * as well as printing to console in a readable way.
 */
public abstract class TaleObject {
    public abstract byte[] serialize();
    public abstract ObjectType getType();
    public abstract TaleObject deserialize(final byte[] serialized);
    public abstract String toCli();
    public abstract int hashCode();
    public abstract boolean equals(Object obj);
}
