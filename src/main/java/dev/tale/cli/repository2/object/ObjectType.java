package dev.tale.cli.repository2.object;

public enum ObjectType {
    BLOB("blob"),
    TREE("tree"),
    COMMIT("commit");

    public final byte[] prefix;
    public final String strPrefix;

    ObjectType(String prefix) {
        this.prefix = prefix.getBytes();
        this.strPrefix = prefix;
    }

    public static ObjectType getType(final byte[] serialized) {
        if(prefixMatches(BLOB, serialized)) {
            return BLOB;
        }
        if(prefixMatches(TREE, serialized)) {
            return TREE;
        }
        if(prefixMatches(COMMIT, serialized)) {
            return COMMIT;
        }
        throw new IllegalArgumentException("Could not determine object type of byte[" + deserializeFirstPart(serialized) + "]");
    }

    private static String deserializeFirstPart(final byte[] serialized) {
        byte[] buf = new byte[20];
        System.arraycopy(serialized, 0, buf, 0, buf.length);
        return new String(buf);
    }

    private static boolean prefixMatches(ObjectType type, final byte[] serialized) {
        return type.prefix[0] == serialized[0] &&
                type.prefix[1] == serialized[1] &&
                type.prefix[2] == serialized[2] &&
                type.prefix[3] == serialized[3];
    }
}
