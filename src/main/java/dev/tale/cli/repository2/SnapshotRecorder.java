package dev.tale.cli.repository2;

import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository2.index.Index2;
import dev.tale.cli.repository2.index.IndexEntry;
import dev.tale.cli.repository2.object.ObjectType;
import dev.tale.cli.repository2.object.Tree;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class SnapshotRecorder implements ServiceProvider<SnapshotRecorder> {
    private static final Path TALE_PATH = Paths.get(".tale");
    private static final String DIR_SEP = "/";

    @Inject
    private Index2 index;

    @Inject
    private ObjectWriter writer;

    private Map<String, List<IndexEntry>> consolidatedBlobs = new HashMap<>();

    @Override
    public void init() {
        // NOP
    }

    @Override
    public SnapshotRecorder getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{Index2.class, ObjectWriter.class};
    }

    /**
     * @return objectName for the generated (and saved) tree.
     */
    public String recordState() {
        consolidatedBlobs = consolidateBlobs();
        Level root = recreateFolderStructure();
        return createAndWriteTrees(root);
    }

    private String createAndWriteTrees(Level parent) {
        Tree.TreeBuilder builder = Tree.builder();

        for(Level child : parent.children) {
            String tree = createAndWriteTrees(child);
            builder.addChild(new Tree.Entry(ObjectType.TREE, tree, child.name));
        }

        List<IndexEntry> indexEntries = consolidatedBlobs.get(parent.fullName);

        if(indexEntries != null) {
            for(IndexEntry entry : indexEntries) {
                builder.addChild(new Tree.Entry(ObjectType.BLOB, entry.getObject(), onlyTail(entry.getName())));
            }
        }

        return writer.writeObject(builder.build());
    }

    private Level recreateFolderStructure() {
        Set<String> allLevels = buildAllLevels(consolidatedBlobs.keySet());
        Map<String, Level> files = new HashMap<>();
        allLevels.forEach(path -> {
            files.put(path, new Level(path, onlyTail(path)));
        });
        files.entrySet().stream() //
                .filter(entry -> !entry.getKey().equals("")) // not interested in root
                .forEach(entry -> {
                    files.get(cutTail(entry.getKey())).addChild(entry.getValue());
                });

        // empty path is the root file
        return files.get("");
    }

    private Set<String> buildAllLevels(Set<String> paths) {
        Set<String> result = new HashSet<>(paths);
        Set<String> shortened = paths.stream() //
            .map(this::cutTail) //
            .filter(s -> !s.isEmpty()) //
            .collect(Collectors.toSet());
        if(!shortened.isEmpty()) {
            result.addAll(buildAllLevels(shortened));
        }
        return result;
    }

    private Map<String, List<IndexEntry>> consolidateBlobs() {
        return index.getAllEntries().stream() //
                .collect(Collectors.groupingBy( //
                        e -> cutTail(e.getName()), //
                        Collectors.toList()));
    }

    /**
     * cuts off the tail of a path.
     * eg.:
     * src/main/Hello.java -> src/main
     * src/main -> src
     * src -> ""
     */
    private String cutTail(String path) {
        if(!path.contains(DIR_SEP)) {
            return "";
        }
        return path.substring(0, path.lastIndexOf(DIR_SEP));
    }

    /**
     * Keeps only the tail of a path
     * eg.:
     * src/main/Hello.java -> Hello.java
     * src/main -> main
     * src -> src
     */
    private String onlyTail(String path) {
        if(!path.contains(DIR_SEP)) {
            return path;
        }
        return path.substring(path.lastIndexOf(DIR_SEP) + 1);
    }

    private static class Level {
        public final String fullName;
        public final String name;
        public final Set<Level> children = new HashSet<>();

        public Level(final String fullName, final String name) {
            this.fullName = fullName;
            this.name = name;
        }

        public void addChild(Level child) {
            this.children.add(child);
        }
    }

}
