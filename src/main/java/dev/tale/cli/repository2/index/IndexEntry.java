package dev.tale.cli.repository2.index;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;

import static dev.tale.cli.function.HashService.DIGEST_LENGTH;

@Data
@EqualsAndHashCode
@AllArgsConstructor
public class IndexEntry {
    private String name;
    private Stage stage;
    private String object;

    public void serialize(ByteArrayOutputStream outputStream) throws IOException {
        outputStream.write(object.getBytes());
        outputStream.write(stage.stage);
        outputStream.write(name.getBytes());
    }

    public static IndexEntry deserialize(byte[] bytes) {
        String object = new String(Arrays.copyOfRange(bytes, 0, DIGEST_LENGTH));
        Stage stage = Stage.fromByte(bytes[DIGEST_LENGTH]);
        String name = new String(Arrays.copyOfRange(bytes, DIGEST_LENGTH + 1, bytes.length));
        return new IndexEntry(name, stage, object);
    }
}
