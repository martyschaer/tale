package dev.tale.cli.repository2.index;

import dev.tale.cli.datastructures.Pair;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.exceptions.ApplicationException;
import dev.tale.cli.function.FileService;
import dev.tale.cli.function.HashService;
import dev.tale.cli.function.ZipService;
import dev.tale.cli.repository.Index;
import dev.tale.cli.repository2.serialization.ByteArraySpliterator;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static dev.tale.cli.repository2.serialization.SerializationHelper.NULL_BYTE;

public class Index2 implements ServiceProvider<Index2> {
    private static final Path INDEX_PATH = Paths.get(".tale", "index");

    @Inject
    FileService fileService;

    @Inject
    ZipService zipService;

    // TODO! this will not work when we implement merging!
    private final Map<String, IndexEntry> cache = new HashMap<>();

    @Override
    public void init() {
        read();
    }

    @Override
    public Index2 getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{FileService.class, ZipService.class};
    }

    public void putState(String name, String objectName) {
        putState(name, objectName, Stage.NORMAL);
    }

    public void putState(String name, String objectName, Stage stage) {
        cache.put(name, new IndexEntry(name, stage, objectName));
        write();
    }

    public Collection<IndexEntry> getAllEntries() {
        return cache.values();
    }

    private void read() {
        byte[] compressed = fileService.load(INDEX_PATH);

        if(compressed.length == 0) {
            return;
        }

        byte[] rawBytes = zipService.inflate(compressed);

        Spliterator<byte[]> spliterator = new ByteArraySpliterator(rawBytes, NULL_BYTE);

        while (spliterator.tryAdvance(bytes -> {
            IndexEntry entry = IndexEntry.deserialize(bytes);
            cache.put(entry.getName(), entry);
        })){ }
    }

    private void write() {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

            for (IndexEntry entry : cache.values()) {
                entry.serialize(outputStream);
                outputStream.write(NULL_BYTE);
            }

            byte[] bytes = outputStream.toByteArray();
            byte[] compressed = zipService.deflate(bytes);
            fileService.write(INDEX_PATH, compressed);
        }catch (IOException e) {
            throw new ApplicationException("Couldn't write to index", e);
        }
    }
}
