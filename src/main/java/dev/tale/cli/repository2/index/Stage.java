package dev.tale.cli.repository2.index;

public enum Stage {
    NORMAL(1),
    COMMON(2),
    OURS(3),
    THEIRS(4);

    public final byte stage;

    Stage(int stage) {
        this.stage = (byte) stage;
    }

    public static Stage fromByte(byte b) {
        if(NORMAL.stage == b) {
            return NORMAL;
        }
        if(COMMON.stage == b) {
            return COMMON;
        }
        if(OURS.stage == b) {
            return OURS;
        }
        if (THEIRS.stage == b) {
            return THEIRS;
        }
        throw new IllegalArgumentException(Integer.toString(b) + "is not a stage.");
    }
}
