package dev.tale.cli.repository2;

import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.function.*;
import dev.tale.cli.repository2.object.TaleObject;

import java.nio.file.Path;

public class ObjectWriter implements ServiceProvider<ObjectWriter> {
    @Inject
    private HashService hashService;

    @Inject
    private HexEncoder hexEncoder;

    @Inject
    private FileService fileService;

    @Inject
    private ObjectNameHandler objectNameHandler;

    @Inject
    private ZipService zipService;

    @Override
    public void init() {
        // NOP
    }

    @Override
    public ObjectWriter getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{HashService.class, HexEncoder.class, //
                FileService.class, ObjectNameHandler.class, //
                ZipService.class};
    }

    /**
     * Serializes the given object (@see {@link TaleObject#serialize()})
     * Then computes the digest of the serialized bytes to form the object name.
     * Deflates the object and writes it to disk.
     *
     * @param object
     * @return the object name
     */
    public String writeObject(TaleObject object) {
        byte[] bytes = object.serialize();
        byte[] deflated = zipService.deflate(bytes);

        String objectName = hexEncoder.toHexString(hashService.hash(bytes));
        Path objectPath = objectNameHandler.toPath(objectName);

        fileService.write(objectPath, deflated);
        return objectName;
    }
}
