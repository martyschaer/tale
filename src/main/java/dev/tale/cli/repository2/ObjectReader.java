package dev.tale.cli.repository2;

import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.exceptions.ApplicationException;
import dev.tale.cli.function.FileService;
import dev.tale.cli.function.ObjectNameHandler;
import dev.tale.cli.function.ZipService;
import dev.tale.cli.repository2.object.*;

import java.nio.file.Path;
import java.util.Arrays;

public class ObjectReader implements ServiceProvider<ObjectReader> {
    @Inject
    private FileService fileService;

    @Inject
    private ObjectNameHandler objectNameHandler;

    @Inject
    private ZipService zipService;

    @Override
    public void init() {

    }

    @Override
    public ObjectReader getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{FileService.class, ZipService.class, ObjectNameHandler.class};
    }

    public TaleObject readObject(String objectName) {
        byte[] bytes = readAndInflateBytes(objectName);
        ObjectType type = determineType(bytes);
        return readObject(type, bytes);
    }

    public TaleObject readObject(ObjectType type, String objectName) {
        byte[] bytes = readAndInflateBytes(objectName);
        return readObject(type, bytes);
    }

    private TaleObject readObject(ObjectType type, byte[] bytes) {
        switch (type) {
            case BLOB:
                return new Blob().deserialize(bytes);
            case TREE:
                return new Tree().deserialize(bytes);
            case COMMIT:
                return new Commit().deserialize(bytes);
            default:
                throw new ApplicationException("Could not deserialize object.");
        }
    }

    public ObjectType determineType(String objectName) {
        return determineType(readAndInflateBytes(objectName));
    }

    private ObjectType determineType(final byte[] bytes) {
        if(startsWith(bytes, ObjectType.BLOB.prefix)) {
            return ObjectType.BLOB;
        }
        if(startsWith(bytes, ObjectType.TREE.prefix)) {
            return ObjectType.TREE;
        }
        if(startsWith(bytes, ObjectType.COMMIT.prefix)) {
            return ObjectType.COMMIT;
        }
        throw new ApplicationException("Couldn't determine type of object");
    }

    private boolean startsWith(final byte[] base, final byte[] prefix) {
        for(int i = 0; i < prefix.length; i++) {
            if(base[i] != prefix[i]) {
                return false;
            }
        }
        return true;
    }

    private byte[] readAndInflateBytes(String objectName) {
        Path objectPath = objectNameHandler.toPath(objectName);
        byte[] compressed = fileService.load(objectPath);
        return zipService.inflate(compressed);
    }
}
