package dev.tale.cli.datastructures;

import lombok.Value;

import java.util.Objects;

@Value
public class Pair<T, U> implements Comparable<Pair<T, U>> {
    private final T left;
    private final U right;

    @Override
    public int compareTo(Pair<T, U> tuPair) {
        if (tuPair == null) {
            return 0;
        }
        int comp = this.left.toString().compareTo(tuPair.left.toString());
        return comp != 0 //
                ? comp //
                : this.right.toString().compareTo(tuPair.right.toString()); //
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Pair<?, ?> pair = (Pair<?, ?>) o;

        if (!Objects.equals(left, pair.left)) return false;
        return Objects.equals(right, pair.right);
    }

    @Override
    public int hashCode() {
        int result = left != null ? left.hashCode() : 0;
        result = 31 * result + (right != null ? right.hashCode() : 0);
        return result;
    }
}
