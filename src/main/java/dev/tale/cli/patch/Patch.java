package dev.tale.cli.patch;

import dev.tale.cli.datastructures.Pair;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;
import lombok.Builder;
import lombok.Value;
import picocli.CommandLine;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Value
@Builder
public class Patch implements CLISerializable, Reversible<Patch> {
    public static final String FROM_HEADER = "--- ";
    public static final String TO_HEADER = "+++ ";

    public Path from;
    public Path to;
    List<Hunk> hunks;

    @Override
    public String toCLIRepresentation() {
        StringBuilder sb = new StringBuilder();
        sb.append(FROM_HEADER).append(PathUtil.relativize(from).toString()).append(System.lineSeparator());
        sb.append(TO_HEADER).append(PathUtil.relativize(to).toString()).append(System.lineSeparator());

        String header = sb.toString();
        sb = new StringBuilder();

        sb.append(CommandLine.Help.Ansi.ON.string("@|bold " + header + "|@"));

        hunks.stream().sorted(Comparator.reverseOrder()) //
                .map(Hunk::toCLIRepresentation) //
                .forEach(sb::append);

        return sb.toString();
    }

    public static Patch fromCLIRepresentation(List<String> input) {
        PatchBuilder builder = Patch.builder();

        String from = input.get(0).substring(FROM_HEADER.length());
        String to = input.get(1).substring(TO_HEADER.length());

        from = FileUtil.unsanitize(from).trim();
        to = FileUtil.unsanitize(to).trim();

        builder.from(Paths.get(from));
        builder.to(Paths.get(to));

        List<Hunk> hunks = new LinkedList<>();
        Hunk currentHunk = null;

        for (int i = 2; i < input.size(); i++) {
            String line = input.get(i);
            if (line.charAt(0) == Hunk.HEADER) {
                if (currentHunk != null) {
                    hunks.add(currentHunk);
                }
                currentHunk = new Hunk(HunkHeader.fromString(line));
            }
            if (currentHunk != null && line.charAt(0) != Hunk.HEADER) {
                currentHunk.addContent(line);
            }
        }
        hunks.add(currentHunk);
        builder.hunks(hunks);

        return builder.build();
    }

    public boolean isForFile(Pair<Path, Path> file) {
        return file.getLeft().equals(this.getFrom()) && file.getRight().equals(this.getTo());
    }

    @Override
    public Patch reverse() {
        List<Hunk> reversedHunks = this.hunks.stream() //
            .map(hunk -> new Hunk(hunk.getHeader().reverse(), hunk.reverse())) //
            .collect(Collectors.toList());
        return new Patch(to, from, reversedHunks);
    }
}
