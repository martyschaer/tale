package dev.tale.cli.patch;

public interface CLISerializable {
    String toCLIRepresentation();
}
