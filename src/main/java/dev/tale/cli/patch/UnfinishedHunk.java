package dev.tale.cli.patch;

import java.util.LinkedList;

/**
 * Represents a Hunk in the making.
 */
public class UnfinishedHunk {
    private LinkedList<PatchLine> patchLines = new LinkedList<>();
    private HunkHeader header;

    public UnfinishedHunk addContentFirst(PatchLine content) {
        this.patchLines.addFirst(content);
        return this;
    }

    public UnfinishedHunk addContent(PatchLine content) {
        this.patchLines.addLast(content);
        return this;
    }

    public UnfinishedHunk header(HunkHeader header) {
        this.header = header;
        return this;
    }

    public LinkedList<PatchLine> content() {
        return this.patchLines;
    }

    /**
     * Does the underlying hunk contain any actual differences or just context
     */
    public boolean isSignificant() {
        return !this.patchLines.stream().allMatch(PatchLine::isSame);
    }

    public Hunk build() {
        Hunk hunk = new Hunk(this.header);
        patchLines.stream() //
                .map(PatchLine::getContent) //
                .forEach(hunk::addContent);

        return hunk;
    }
}
