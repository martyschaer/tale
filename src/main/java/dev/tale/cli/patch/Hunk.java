package dev.tale.cli.patch;

import dev.tale.cli.util.FileUtil;
import lombok.Value;
import picocli.CommandLine;

import java.util.LinkedList;
import java.util.stream.Collectors;

@Value
public class Hunk implements Comparable<Hunk>, CLISerializable, Reversible<LinkedList<String>> {
    public static final char ADD = '+';
    public static final char REMOVE = '-';
    public static final char SAME = ' ';
    public static final char HEADER = '@';

    private final HunkHeader header;
    private final LinkedList<String> content;

    public Hunk(HunkHeader header) {
        this(header, new LinkedList<>());
    }

    public Hunk(HunkHeader header, LinkedList<String> content) {
        this.header = header;
        this.content = content;
    }

    public void addContent(String content) {
        this.content.add(content);
    }

    @Override
    public int compareTo(Hunk that) {
        return this.getHeader().compareTo(that.getHeader());
    }


    @Override
    public String toCLIRepresentation() {
        StringBuilder sb = new StringBuilder();
        sb.append(CommandLine.Help.Ansi.ON.string("@|blue " + header.toCLIRepresentation() + "|@"));
        content.stream() //
                .map(FileUtil::unsanitize) //
                .map(Hunk::colorHunkLine) //
                .forEach(sb::append);
        return sb.toString();
    }

    /**
     * Color a hunk line depending on type.
     *
     * @param line The line to color
     * @return The colored line
     */
    private static String colorHunkLine(String line) {
        switch (line.charAt(0)) {
            case ADD:
                return CommandLine.Help.Ansi.ON.string("@|green " + line + "|@");
            case REMOVE:
                return CommandLine.Help.Ansi.ON.string("@|red " + line + "|@");
            default:
                return line;
        }
    }

    @Override
    public LinkedList<String> reverse() {
        return content.stream() //
            .map(line -> {
                if(line.charAt(0) == ADD) {
                    return REMOVE + line.substring(1);
                } else if(line.charAt(0) == REMOVE) {
                    return ADD + line.substring(1);
                } else {
                    return line;
                }
            })//
            .collect(Collectors.toCollection(LinkedList::new));
    }
}
