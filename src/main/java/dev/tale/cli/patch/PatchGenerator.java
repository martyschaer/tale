package dev.tale.cli.patch;

import dev.tale.cli.datastructures.Vertex;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class PatchGenerator {

    /**
     * For testing purposes only.
     */
    @Deprecated
    static Patch generate(List<Vertex> editPath, List<String> from, List<String> to) {
        Patch.PatchBuilder builder = Patch.builder();
        builder.from(Paths.get("b/"));
        builder.to(Paths.get("a/"));
        builder.hunks(generateHunks(editPath, 3, from, to));
        return builder.build();
    }

    public static Patch generate(Path fromPath, Path toPath) {
        List<String> from = PathUtil.isNullEquivalent(fromPath) ? new LinkedList<>() : FileUtil.loadFileFromPath(fromPath);
        List<String> to = PathUtil.isNullEquivalent(toPath) ? new LinkedList<>() : FileUtil.loadFileFromPath(toPath);

        return generate(fromPath, toPath, from, to);
    }

    public static Patch generate(Path fromPath, Path toPath, List<String> from, List<String> to) {
        DifferenceAlgorithm differ = new DifferenceAlgorithm(from, to);
        List<Vertex> editPath = differ.run();

        Patch.PatchBuilder builder = Patch.builder();
        builder.from(fromPath);
        builder.to(toPath);
        builder.hunks(generateHunks(editPath, 3, from, to));
        return builder.build();
    }

    private static List<Hunk> generateHunks(List<Vertex> editPath, int linesOfContext,
                                            List<String> from, List<String> to) {
        LinkedList<PatchLine> patchLines = getUnifiedFormatLines(editPath, from, to);

        LinkedList<UnfinishedHunk> hunks = new LinkedList<>();
        UnfinishedHunk currentHunk = new UnfinishedHunk();

        int lastLineWithDiff = 0;
        for (int i = 0; i < patchLines.size(); i++) {
            if (lastLineWithDiff + linesOfContext < i) {
                hunks.add(currentHunk);
                currentHunk = new UnfinishedHunk();

                // init hunk with the previous three lines of context
                for (int b = 1; b <= linesOfContext && i - b >= 0; b++) {
                    currentHunk.addContentFirst(patchLines.get(i - b));
                }
            }

            PatchLine patchLine = patchLines.get(i);
            currentHunk.addContent(patchLine);
            if (!patchLine.isSame()) {
                lastLineWithDiff = i;
            }
        }

        // add last in-progress hunk
        hunks.add(currentHunk);

        hunks = hunks.stream() //
                .filter(UnfinishedHunk::isSignificant) //
                .collect(Collectors.toCollection(LinkedList::new));

        hunks.forEach(h -> h.header(HunkHeader.createHeaderFromContent(h.content())));

        return hunks.stream().map(UnfinishedHunk::build).collect(Collectors.toList());
    }

    private static LinkedList<PatchLine> getUnifiedFormatLines(List<Vertex> editPath, List<String> from, List<String> to) {
        LinkedList<PatchLine> diff = new LinkedList<>();

        for (int i = 1; i < editPath.size(); i++) {
            Vertex curr = editPath.get(i - 1);
            Vertex next = editPath.get(i);

            if (isInsert(curr, next)) {
                diff.addFirst(new PatchLine(curr, "+" + to.get(curr.y - 1)));
            } else if (isDelete(curr, next)) {
                diff.addFirst(new PatchLine(curr, "-" + from.get(curr.x - 1)));
            } else {
                diff.addFirst(new PatchLine(curr, " " + from.get(curr.x - 1), true));
            }
        }
        return diff;
    }

    private static boolean isInsert(Vertex current, Vertex next) {
        return next.x == current.x && next.y < current.y;
    }

    private static boolean isDelete(Vertex current, Vertex next) {
        return next.x < current.x && next.y == current.y;
    }
}
