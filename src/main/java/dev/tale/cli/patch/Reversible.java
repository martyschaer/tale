package dev.tale.cli.patch;

public interface Reversible <T> {
    T reverse();
}
