package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.ServiceProvider;

public class StatusImpl implements ServiceProvider<StatusImpl> {

    @Override
    public void init() {
        // NOP
    }

    @Override
    public StatusImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[] {};
    }

    public Integer printStatus() {
        return ExitCode.SUCCESS;
    }
}
