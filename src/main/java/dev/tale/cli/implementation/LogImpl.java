package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository.Reference;
import dev.tale.cli.repository.TreeAccessor;
import dev.tale.cli.repository.object.TreeObject;

public class LogImpl implements ServiceProvider<LogImpl> {

    @Inject
    private TreeAccessor treeAccessor;

    @Override
    public void init() {

    }

    @Override
    public LogImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[] {TreeAccessor.class};
    }

    public Integer printHistory(String id, boolean oneLine, boolean verbose) {
        if (id == null) {
            System.out.println("History doesn't have any commits yet.");
            return ExitCode.ERROR;
        }

        if (id.equals(Reference.NULL_REFERENCE)) {
            return ExitCode.ERROR;
        }

        TreeObject currentObject = treeAccessor.getTreeObject(id);
        System.out.println(currentObject.getLogEntry(oneLine, verbose));
        printHistory(currentObject.getParentID(), oneLine, verbose);

        return ExitCode.SUCCESS;
    }
}
