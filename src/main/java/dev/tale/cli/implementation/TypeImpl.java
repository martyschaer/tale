package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository2.ObjectReader;
import dev.tale.cli.repository2.object.ObjectType;

public class TypeImpl implements ServiceProvider<TypeImpl> {

    @Inject
    private ObjectReader reader;

    @Override
    public void init() {

    }

    @Override
    public TypeImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{ObjectReader.class};
    }

    public Integer showType(String objectName) {
        ObjectType type = reader.determineType(objectName);
        System.out.println(type.name().toLowerCase());
        return ExitCode.SUCCESS;
    }
}
