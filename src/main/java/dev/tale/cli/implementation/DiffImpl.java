package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.datastructures.Pair;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.patch.Patch;
import dev.tale.cli.patch.PatchGenerator;
import dev.tale.cli.repository.History;
import dev.tale.cli.repository.Index;
import dev.tale.cli.repository.Repository;
import dev.tale.cli.util.FileUtil;
import dev.tale.cli.util.PathUtil;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class DiffImpl implements ServiceProvider<DiffImpl> {
    @Inject
    private History history;

    @Inject
    private Index index;

    @Inject
    private Repository repository;

    @Override
    public void init() {

    }

    @Override
    public DiffImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class<?>[] {
                History.class,
                Index.class,
                Repository.class
        };
    }

    public Integer indexDiff(Path[] paths) {
        if(paths.length == 0) {
            //
            paths = repository.listChangedFiles().stream() //
                    .filter(p -> p.getLeft().equals(p.getRight())) //
                    .map(Pair::getLeft) //
                    .toArray(Path[]::new);
        }

        return Arrays.stream(paths) //
                .map(PathUtil::relativize) //
                .map(this::diffFile) //
                .allMatch(i -> i.equals(ExitCode.SUCCESS)) ? ExitCode.SUCCESS : ExitCode.ERROR;
    }

    private Integer diffFile(Path path) {
        String id = index.getState(path);

        List<String> previousContent = history.getHistoricState(path, id);

        Patch patch = PatchGenerator.generate(path, path, previousContent, FileUtil.loadFileFromPath(path));

        System.out.println(patch.toCLIRepresentation());

        return ExitCode.SUCCESS;
    }
}
