package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.repository2.ObjectReader;
import dev.tale.cli.repository2.object.ObjectType;
import dev.tale.cli.repository2.object.TaleObject;

public class CatImpl implements ServiceProvider<CatImpl> {

    @Inject
    private ObjectReader reader;

    @Override
    public void init() {

    }

    @Override
    public CatImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{ObjectReader.class};
    }

    public Integer catObject(String objectName) {
        TaleObject taleObject = reader.readObject(objectName);
        System.out.println(taleObject.toCli());
        return ExitCode.SUCCESS;
    }
}
