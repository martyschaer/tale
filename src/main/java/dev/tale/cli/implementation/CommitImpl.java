package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.function.BranchNameHandler;
import dev.tale.cli.function.Config;
import dev.tale.cli.function.FileService;
import dev.tale.cli.repository2.ObjectWriter;
import dev.tale.cli.repository2.SnapshotRecorder;
import dev.tale.cli.repository2.object.Commit;

public class CommitImpl implements ServiceProvider<CommitImpl> {

    @Inject
    private SnapshotRecorder recorder;

    @Inject
    private BranchNameHandler branchNameHandler;

    @Inject
    private Config config;

    @Inject
    private ObjectWriter writer;

    @Inject
    private FileService fileService;

    @Override
    public void init() {
    }

    @Override
    public CommitImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class<?>[]{SnapshotRecorder.class, BranchNameHandler.class, //
                Config.class, ObjectWriter.class, //
                FileService.class //
        };
    }

    public Integer commit(String message) {
        String tree = recorder.recordState();

        Commit commit = new Commit();
        commit.setParent1(branchNameHandler.getHEAD());
        commit.setAuthor(config.getUser());
        commit.setMessage(message);
        commit.setTree(tree);
        commit.setTimestampToNow();

        String commitId = writer.writeObject(commit);
        branchNameHandler.putHEAD(commitId);

        // TODO choose commitId length dynamically
        System.out.printf("[%s %s] %s %s\n",
                branchNameHandler.getCurrentBranch(),
                commitId.substring(0, 10),
                commit.getFormattedTimestamp(),
                message);
        return ExitCode.SUCCESS;
    }
}
