package dev.tale.cli.implementation;

import dev.tale.cli.command.ExitCode;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.function.FileService;
import dev.tale.cli.repository2.ObjectWriter;
import dev.tale.cli.repository2.index.Index2;
import dev.tale.cli.repository2.object.Blob;

import java.nio.file.Path;

public class AddImpl implements ServiceProvider<AddImpl> {

    @Inject
    FileService fileService;

    @Inject
    ObjectWriter writer;

    @Inject
    Index2 index;

    @Override
    public void init() {

    }

    @Override
    public AddImpl getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{FileService.class, ObjectWriter.class, Index2.class};
    }

    public Integer addPath(Path path) {
        byte[] fileBytes = fileService.load(path);
        Blob blob = new Blob(fileBytes);
        String object = writer.writeObject(blob);
        index.putState(path.toString(), object);

        return ExitCode.SUCCESS;
    }
}
