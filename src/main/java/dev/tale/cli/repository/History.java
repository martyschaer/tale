package dev.tale.cli.repository;

import dev.tale.cli.datastructures.Pair;
import dev.tale.cli.datastructures.SizeLimitedCache;
import dev.tale.cli.di.Inject;
import dev.tale.cli.di.ServiceProvider;
import dev.tale.cli.patch.Patch;
import dev.tale.cli.patch.Patcher;
import dev.tale.cli.repository.object.Content;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.stream.Collectors;

public class History implements ServiceProvider<History> {
    @Inject
    private TreeAccessor treeAccessor;

    @Inject
    private Index index;

    private SizeLimitedCache<Pair<String, Path>, List<String>> cache;

    @Override
    public void init() {
        cache = new SizeLimitedCache<>(32);
    }

    @Override
    public History getInstance() {
        return this;
    }

    @Override
    public Class<?>[] getDependencies() {
        return new Class[]{Index.class, TreeAccessor.class};
    }

    public List<String> getHistoricState(Path path, String id) {
        return this.cache.computeIfAbsent(new Pair<>(id, path), k -> {
            Stack<String> history = new Stack<>();

            history.push(id);
            history = reverse(Reference.resolveHierarchy(Reference.toPath(id), history));

            List<String> previousContent = new ArrayList<>();

            for(String refId : history) {
                if(refId == null){
                    continue;
                }

                Content content = treeAccessor.getContent(refId);

                List<Patch> patches = content.getPatches().stream() //
                        .filter(patch -> patch.getTo().equals(path)) //
                        .collect(Collectors.toList()); //
                patches.forEach(patch -> {
                    Patcher patcher = new Patcher(patch.getHunks());
                    patcher.applyTo(previousContent);
                });
            }

            return previousContent;
        });
    }

    private Stack<String> reverse(Stack<String> input) {
        Stack<String> result = new Stack<>();
        while(!input.empty()) {
            result.push(input.pop());
        }

        return result;
    }
}
