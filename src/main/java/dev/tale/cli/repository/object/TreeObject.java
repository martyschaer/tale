package dev.tale.cli.repository.object;

import dev.tale.cli.config.Config;
import dev.tale.cli.exceptions.ApplicationException;
import dev.tale.cli.patch.Patch;
import dev.tale.cli.repository.Reference;
import dev.tale.cli.repository.TreeAccessor;
import dev.tale.cli.util.EncodingUtil;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import picocli.CommandLine;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.Instant;
import java.time.ZoneId;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
public class TreeObject {
    private static final int ID_LENGTH = 64;
    private static final String NEWLINE = System.lineSeparator();
    private static final String SEPARATOR = ",";

    private String parentID;
    private String parent2ID;
    @Getter(AccessLevel.NONE)
    private long timestamp;
    private String content;
    private File file;

    public TreeObject(String parentID, long timestamp, File file) {
        this.parentID = parentID;
        this.parent2ID = null;
        this.timestamp = timestamp;
        this.content = "";
        this.file = file;
    }

    public TreeObject(String parentID, String parent2ID, long timestamp, String content, File file) {
        this.parentID = parentID;
        this.parent2ID = parent2ID;
        this.timestamp = timestamp;
        this.content = content;
        this.file = file;
    }

    public void updateTimestamp() {
        this.timestamp = System.currentTimeMillis();
    }

    public static TreeObject read(File file) {
        if (!file.canRead()) {
            throw new ApplicationException("Not a readable file: " + file.getAbsolutePath());
        }
        TreeObjectBuilder builder = TreeObject.builder();
        builder.file(file);

        try {
            List<String> lines = Files.readAllLines(file.toPath());

            String[] ids = lines.get(0).split(",");
            builder.parentID(ids[0]);
            if (ids.length == 2) {
                builder.parent2ID(ids[1]);
            }

            builder.timestamp(Long.parseLong(lines.get(1)));

            builder.content(lines.get(2));

            return builder.build();
        } catch (IOException e) {
            throw new ApplicationException("Couldn't read file: " + file.getAbsolutePath());
        }
    }

    public String hash() {
        return EncodingUtil.hash(toDiskRepresentation());
    }

    public String toDiskRepresentation() {
        return new StringBuilder() //
                .append(this.parentID == null ? Reference.NULL_REFERENCE : this.parentID) //
                .append(this.parent2ID == null ? "" : SEPARATOR + this.parent2ID).append(NEWLINE) //
                .append(this.timestamp).append(NEWLINE) //
                .append(this.content).append(NEWLINE) //
                .toString();
    }

    public byte[] toDiskBytes() {
        return this.toDiskRepresentation().getBytes();
    }

    public Content getContentAsObject() {
        return new Content(this.getContent());
    }

    /**
     * Get the timestamp of the object in seconds.
     *
     * @return The timestamp in seconds.
     */
    private long getTimestampSeconds() {
        return this.timestamp / 1000;
    }

    /**
     * Get the timestamp of the object as a formatted string.
     *
     * @return The formatted timestamp
     */
    private String getFormattedTimestamp() {
        return Instant.ofEpochSecond(this.getTimestampSeconds())
                .atZone(ZoneId.systemDefault())
                .format(Config.getInstance().getDateFormatter());
    }

    /**
     * Get all the patches for this object as a string.
     *
     * @return The patches as a string
     */
    public String getPatches() {
        return this.getContentAsObject()
                .getPatches()
                .stream()
                .map(Patch::toCLIRepresentation)
                .collect(Collectors.joining("\n"));
    }

    /**
     * Get the log entry for this object.
     *
     * @param isOneline Whether to display to entry only on one line
     * @param isVerbose Whether to display the entire id
     * @return The log entry for this object
     */
    public String getLogEntry(boolean isOneline, boolean isVerbose) {
        String id = isVerbose ? this.hash() : this.hash().substring(0, TreeAccessor.getMinIdLength());
        Content content = this.getContentAsObject();

        String branchString = "";
        if (TreeAccessor.getHeadId().equals(id)) {
            branchString = CommandLine.Help.Ansi.ON.string("@|yellow (|@@|blue HEAD|@@|yellow )|@ ");
        }

        if (isOneline) {
            return String.format(
                    CommandLine.Help.Ansi.ON.string("@|yellow %s|@ %s%s"),
                    id,
                    branchString,
                    content.getMessage()
            );
        }

        return String.format(
                CommandLine.Help.Ansi.ON.string("@|yellow Commit: %s|@ %s%nAuthor: %s%nDate:   %s%n%n        %s%n"),
                id,
                branchString,
                content.getAuthor(),
                this.getFormattedTimestamp(),
                content.getMessage()
        );

    }
}
