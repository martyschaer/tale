package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.DiffImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

import java.nio.file.Path;
import java.util.Objects;

@CommandLine.Command(
        name = "diff",
        description = "Shows changes between files, commits, etc.",
        subcommands = { NoIndexDiffCommand.class })
public class DiffCommand extends Subcommand {
    @CommandLine.Parameters(index = "0..*", type = Path.class)
    private Path[] paths;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        DiffImpl impl = ServiceFactory.get(DiffImpl.class);
        return impl.indexDiff(Objects.nonNull(paths) ? paths : new Path[0]);
    }

}
