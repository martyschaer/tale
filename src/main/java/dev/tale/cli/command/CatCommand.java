package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.CatImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

@CommandLine.Command(name = "cat", description = "Shows the content of an object.")
public class CatCommand extends Subcommand {
    @CommandLine.Parameters(index = "0..1", type = String.class)
    private String objectName;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        CatImpl impl = ServiceFactory.get(CatImpl.class);
        if (this.objectName != null) {
            impl.catObject(objectName);
            return ExitCode.SUCCESS;
        }

        return ExitCode.USAGE;
    }

}
