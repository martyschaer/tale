package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.AddImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

import java.nio.file.Path;
import java.util.Arrays;

@CommandLine.Command(name = "add", description = "Adds the specified files to the staging area.")
public class AddCommand extends Subcommand {
    @CommandLine.Parameters(index = "0..*", type = Path.class)
    private Path[] paths;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        AddImpl impl = ServiceFactory.get(AddImpl.class);
        if (this.paths != null) {
            if(Arrays.stream(paths) //
                .map(impl::addPath) //
                .anyMatch(n -> n.equals(ExitCode.ERROR))) {
                return ExitCode.ERROR;
            }
            return ExitCode.SUCCESS;
        }

        return ExitCode.USAGE;
    }

}
