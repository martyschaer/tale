package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.DeleteImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

import java.nio.file.Path;

@CommandLine.Command(name = "rm", description = "Deletes the specified files.")
public class DeleteCommand extends Subcommand {
    @CommandLine.Parameters(index = "0..*", type = Path.class)
    private Path[] paths;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();

        if(paths == null || paths.length == 0) {
            return ExitCode.USAGE;
        }

        DeleteImpl impl = ServiceFactory.get(DeleteImpl.class);
        return impl.delete(paths);
    }
}
