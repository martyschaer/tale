package dev.tale.cli.command;

import picocli.CommandLine;

import java.util.concurrent.Callable;

@CommandLine.Command(
        name = "tale",
        mixinStandardHelpOptions = true,
        version = "tale 0.1.0",
        description = "The version control system of the future.",
        subcommands = {
                CommandLine.HelpCommand.class,
                InitCommand.class,
                AddCommand.class,
                TypeCommand.class,
                CatCommand.class,
                CommitCommand.class,
                StatusCommand.class
        }
)
public class Tale implements Callable<Integer> {
    @Override
    public Integer call() {
        CommandLine.usage(this, System.out);
        return ExitCode.USAGE;
    }
}
