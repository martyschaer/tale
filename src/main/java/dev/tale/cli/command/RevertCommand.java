package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.RevertImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

@CommandLine.Command(name = "revert", description = "Revert some existing commits")
public class RevertCommand extends Subcommand {

    @CommandLine.Parameters(arity = "1")
    private String reference;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        RevertImpl impl = ServiceFactory.get(RevertImpl.class);
        return impl.revert(reference);
    }
}
