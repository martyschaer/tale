package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.TypeImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

@CommandLine.Command(name = "type", description = "Displays the type of the given object")
public class TypeCommand extends Subcommand {
    @CommandLine.Parameters(index = "0..1", type = String.class)
    private String objectName;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        TypeImpl impl = ServiceFactory.get(TypeImpl.class);
        if (this.objectName != null) {
            return impl.showType(objectName);
        }

        return ExitCode.USAGE;
    }

}
