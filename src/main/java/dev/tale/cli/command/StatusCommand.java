package dev.tale.cli.command;

import dev.tale.cli.di.ServiceFactory;
import dev.tale.cli.implementation.StatusImpl;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

@CommandLine.Command(name = "status", description = "Show the status of the repository")
public class StatusCommand extends Subcommand {
    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();
        StatusImpl impl = ServiceFactory.get(StatusImpl.class);
        return impl.printStatus();
    }
}
