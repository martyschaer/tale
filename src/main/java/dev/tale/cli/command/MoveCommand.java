package dev.tale.cli.command;

import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

import java.nio.file.Path;

@CommandLine.Command(name = "mv", description = "Renames the specified file.")
public class MoveCommand extends Subcommand {
    @CommandLine.Parameters(index = "0", paramLabel = "<path - from>")
    Path fromFile;

    @CommandLine.Parameters(index = "1", paramLabel = "<path - to>")
    Path toFile;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();

        if(fromFile == null || toFile == null) {
            return ExitCode.USAGE;
        }

        throw new UnsupportedOperationException("Moving is not yet supported.");
    }
}
