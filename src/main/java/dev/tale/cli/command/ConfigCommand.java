package dev.tale.cli.command;

import dev.tale.cli.config.Config;
import dev.tale.cli.util.RepositoryUtil;
import picocli.CommandLine;

@CommandLine.Command(name = "config", description = "Get and set repository or global options.")
public class ConfigCommand extends Subcommand {
    @CommandLine.Parameters(index = "0")
    private String key;

    @CommandLine.Parameters(index = "1", defaultValue = "")
    private String value;

    @Override
    public Integer call() {
        RepositoryUtil.assertRunningInRepository();

        if (value.isEmpty()) {
            System.out.println(Config.getInstance().get(this.key));
        } else {
            Config.getInstance().set(this.key, this.value);
        }

        return ExitCode.SUCCESS;
    }
}
