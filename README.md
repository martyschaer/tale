<div align="center">

<img src="./assets/logo.png" align="center" width="150" alt="Tale">

# An experimental version control system.

</div>

[![Pipeline](https://gitlab.com/severinkaderli/tale/badges/master/pipeline.svg)](https://gitlab.com/severinkaderli/tale/pipelines)
[![codecov](https://codecov.io/gl/severinkaderli/tale/branch/master/graph/badge.svg)](https://codecov.io/gl/severinkaderli/tale)

## Information
tale was created as part of module BTI7301-Proj1 at the University of Applied Science in Bern.

### Authors
| Author          | GitLab                                               | Website                                        |
|-----------------|------------------------------------------------------|------------------------------------------------|
| Severin Kaderli | [@severinkaderli](https://gitlab.com/severinkaderli) | [severinkaderli.ch](https://severinkaderli.ch) |
| Marius Schär    | [@martyschaer](https://gitlab.com/martyschaer)       | [mariusschaer.ch](https://mariusschaer.ch)     |

### Documentation
The documentations which we created as part of the module can be found below. They are built from the source in the
`./doc` folder.

* [Requirements Specification](https://gitlab.com/severinkaderli/tale/-/jobs/386873952/artifacts/file/requirements-specification.pdf)
* [Final Report](https://gitlab.com/severinkaderli/tale/-/jobs/386873952/artifacts/file/final-report.pdf)

## How to get
### Nightly Build
You can find the latest build for Linux [here](https://gitlab.com/severinkaderli/tale/builds/artifacts/master/raw/target/tale?job=Build).

## Arch Linux
tale can be found in the AUR as [tale-git](https://aur.archlinux.org/packages/tale-git/).

## How to compile
If you want to compile tale yourself you need to have the JDK 8 version of GraalVM, and maven installed.

After setting the GraalVM as the primary Java VM go ahead and run the following command. This will output a tale
executable in the `./target` folder.  
`mvn clean package`
