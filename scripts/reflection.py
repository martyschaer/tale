#!/usr/bin/python3
import json
import subprocess

if __name__ == "__main__":
    file_name = "./target/classes/META-INF/native-image/picocli-generated/dev.tale/cli/reflect-config.json"
    classes = [
        "dev.tale.cli.config.Config",
        "com.electronwill.nightconfig.toml.TomlFormat",
        "com.electronwill.nightconfig.core.conversion.ObjectConverter",
        "com.electronwill.nightconfig.core.file.FileConfig",
        "com.electronwill.nightconfig.core.file.CommentedFileConfig"
    ]

    result = subprocess.run(['grep', '-lrns', '-e', '@Inject', '-e', 'implements ServiceProvider'], stdout=subprocess.PIPE)
    files = result.stdout.decode('utf-8').strip().split('\n')

    result = subprocess.run(['find', 'src/main/java/dev/tale/cli/implementation/', '-type', 'f'], stdout=subprocess.PIPE)
    files.append(result.stdout.decode('utf-8').strip().split('\n'))


    for file in files:
        if '/main/' in file:
            with open(file) as content:
                package = content.readlines()[0][8:-2]
                clazz = package + '.' + file.split('/')[-1][0:-5]
                classes.append(clazz)

    with open(file_name, "r") as file:
        data = json.load(file)
        for c in classes:
            entry = {
                "name": c,
                "allDeclaredConstructors" : True,
                "allPublicConstructors" : True,
                "allDeclaredMethods" : True,
                "allPublicMethods" : True,
                "allDeclaredClasses" : True,
                "allPublicClasses" : True,
                "allPublicFields" : True,
                "allDeclaredFields" : True
            }

            if 'dev.tale' in c:
                entry['methods'] = [ { "name": "<init>", "parameterTypes": [] } ]

            data.append(entry)

    with open(file_name, "w") as file:
        json.dump(data, file)
